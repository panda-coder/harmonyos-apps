import data from "../../common/data.js"

export default {
    data: {
        videoSource:[],//视频数据源
        isShowController:false,//是否显示控制栏（当流转时默认显示控制栏）
        isAllScreen:false,//是否全屏
        showToolsTime:0,
        isPlayed:false,//是否播放
        totalTime:999999999,//slider的bug先附一个较大值
        watchTimer:-1,
        commentContent:"",
        isChildToolTouched:false,
        listTouchedMoved:[],
        videoTouchMoveTouches:[],
        isAutoPlay:false
    },
    shareData:{
        playIndex:0,//当前播放序号
        thisTime:0,
        playSpeed:1.0,
        commentsSource:[],//评论数据源
    },
    computed:{
        playedVideo(){//当前播放视频
            return this.videoSource[this.playIndex]
        },
        thisTimeStr(){
            let hour=0;
            let min=0;
            let secend=0;
            secend=parseInt(this.thisTime%60)
            min=parseInt((this.thisTime/60)%60)
            hour=parseInt(this.thisTime/3600)
            return `${this.prefixInteger(hour,2)}:${this.prefixInteger(min,2)}:${this.prefixInteger(secend,2)}`
        },
        totalTimeStr(){
            let hour=0;
            let min=0;
            let secend=0;
            secend=parseInt(this.totalTime%60)
            min=parseInt((this.totalTime/60)%60)
            hour=parseInt((this.totalTime/3600))
            return `${this.prefixInteger(hour,2)}:${this.prefixInteger(min,2)}:${this.prefixInteger(secend,2)}`
        },
        thisComments(){
            let data=this.commentsSource.filter(t=>t.videoId==this.playedVideo.id)
            return data.reverse()
        },
        speedStr(){
            if(this.playSpeed==1.0)
                    return "倍速";
            return `${this.playSpeed}倍`
        }

    },
    //根据位数自动往前补零
    prefixInteger(num, m) {
        return (Array(m).join(0) + num).slice(-m);
    },
    onInit() {
        this.$watch("showToolsTime",'watchShowToolsTimeEvent')
        this.videoSource=data.videoSource
        this.commentsSource=data.comments
        this.playIndex=0;
        this.showToolsTime=5;
    },
    childToolTouched(){
        this.isChildToolTouched=true
    },
    mPrepared({duration}){
        this.totalTime=duration
        if(this.thisTime>0){
            this.sliderChanged({value:this.thisTime})
        }
        if(this.isAutoPlay){
            this.$element("m_video").start()
            this.isAutoPlay=false
        }
    },
    //视频滑动事件
    videoTouchMoved(evt){
        if(!this.isChildToolTouched){
            this.videoTouchMoveTouches=this.videoTouchMoveTouches.concat(evt.touches)
        }
    },
    //视频列表touchmove事件，防止拖动时和touchend事件冲突
    listTouchMoved(){
        this.listTouchedMoved.push(1)
    },
    //视频播放事件
    mStart(){
        this.isPlayed=true
    },
    //视频播放速度调整
    speedChanged(speed){
        this.playSpeed=speed;
    },
    //视频停止事件
    mStop(){
        this.isPlayed=false
    },
    //重置播放器
    resetVideo(){
        this.thisTime=0
        this.isPlayed=false
    },
    //视频播放进度变化事件
    timeChanged({currenttime}){
        this.thisTime=currenttime
    },
    toolsTouched(){
        //当已显示时隐藏，当未显示时显示并默认5s自动关闭
        if(this.showToolsTime>0 && !this.childToolTouched){
            this.showToolsTime=0
            this.childToolTouched=false
        }else{
            this.showToolsTime=5
        }
        //复原
        this.childToolTouched=false
        //判单手势
       let touchMoveOrientation= this.getTouchMoveOrientation(this.videoTouchMoveTouches,10);
        this.videoTouchMoveTouches=[];
        switch(touchMoveOrientation){
            case "left2right":{
                this.nextVideo();
                break;
            }
            case "right2left":{
                this.prevVideo();
                break;
            }

        }
    },
    //播放上一个
    prevVideo(){
        let index=(this.playIndex<=0?this.videoSource.length-1:this.playIndex-1);
        this.checkVideo(index);
    },
    //播放下一个
    nextVideo(){
        let index=(this.playIndex>=this.videoSource.length-1?0:this.playIndex+1);
        this.checkVideo(index);
    },
    //判断手势
    getTouchMoveOrientation(source,num){
        let orientation=[]
        for(let i=1;i<source.length;i++){
            let startX=source[i-1].localX
            let startY=source[i-1].localY
            let moveEndX=source[i].localX
            let moveEndY=source[i].localY
            let X=moveEndX-startX;
            let Y=moveEndY-startY;
            if(Math.abs(X)>Math.abs(Y) && X>0){
                orientation.push("left2right")
            }else if(Math.abs(X)>Math.abs(Y) && X<0){
                orientation.push("right2left")
            }else if(Math.abs(X)<Math.abs(Y) && Y>0){
                orientation.push("top2bottom")
            }else if(Math.abs(X)<Math.abs(Y) && Y<0){
                orientation.push("bottom2top")
            }
        }
        let obj={},maxNum=0
        orientation.forEach((item,index)=>{
            if(orientation.indexOf(item)==index){
                obj[item]=1
            }else{
                obj[item]+=1
            }
        })

        for(let i in obj){
            if(obj[i]>maxNum){
                maxNum=obj[i]
            }
        }
        if(maxNum<num)
           return "none"
        for(let i in obj){
            if(obj[i]==maxNum)
                return i
        }
    },
    //播放或暂停
    playOrPause(){
        if(this.isPlayed)
            this.$element("m_video").pause()
        else
            this.$element("m_video").start()
    },
    //全屏播放
    showAllScreen(){
        if(this.isAllScreen)
            this.$element("m_video").exitFullscreen()
        else
            this.$element("m_video").requestFullscreen({ screenOrientation : "default" })
    },
    //进度条改变事件
    sliderChanged({value}){
        this.$element("m_video").setCurrentTime({currenttime:value})
    },
    //选择视频
    checkVideo(index){
        if(this.listTouchedMoved.length>5){
            this.listTouchedMoved=[];
            return;
        }
        if(this.playIndex!=index){
            try{
                this.$element("m_video").stop();//使用该方法api6才有效
            }catch{
                this.sliderChanged({value:0,progress:0})
            }
        }
        this.isAutoPlay=true
        this.playIndex=index;
    },
    //发表评论
    sendComment(){
        let data={
            id:this.commentsSource.length+1,
            user:"匿名用户",
            videoId:this.playedVideo.id,
            content:this.commentContent
        }
        this.commentsSource.push(data)
        this.commentContent=""
    },
    //评论输入框改变事件
    commentChanged({value}){
        this.commentContent=value
    },
    //流转
    remoteIt:async function(){
        await FeatureAbility.continueAbility();
    },
    //展开倍数菜单
    showSpeedMenu(){
        this.$element("speedPopup").show()
    },
    //监听控制栏显示时间
    watchShowToolsTimeEvent(newV,oldV){
        if(newV>0){
            clearTimeout(this.watchTimer)
            this.watchTimer=setTimeout(()=>{
                if(newV>oldV)
                    this.showToolsTime=newV
                --this.showToolsTime;
            },1000)
        }else{
            this.$element("speedPopup").hide()
        }
    },
    /*-------------------------------分布式迁移相关函数----------------------------------------*/
    //判单当前是否适合迁移
    onStartContinuation(){
      return true;
    },
    onSaveData(saveData){
        Object.assign(saveData, this.shareData)
    },
    onCompleteContinuation(code) {
        // 迁移操作完成，code返回结果
        console.info("CompleteContinuation: code = " + code);
    },
    //接收到的迁移数据
    onRestoreData(restoreData){
        this.isAutoPlay=true
        Object.assign(restoreData,this.shareData)
    }

}
