import { jsCallJavaAbility } from '../../common/JsCallJavaAbilityUtils.js';

export default {
    data: {
        text: ""
    },
    start() {
        jsCallJavaAbility.callAbility("ControllerAbility",100,{}).then(result=>{
            console.log(result)
        })
    },
    stop() {
        jsCallJavaAbility.callAbility("ControllerAbility",101,{}).then(result=>{
            console.log(result)
        })
        jsCallJavaAbility.unSubAbility("ControllerAbility",201).then(result=>{
            if (result.code == 200) {
                console.log("取消订阅成功");
            }
        })
    },
    sub() {
        jsCallJavaAbility.subAbility("ControllerAbility", 200, (data) => {
            let text = data.data.text
            text && (this.text += text)
        }).then(result => {
            if (result.code == 200) {
                console.log("订阅成功");
            }
        })
    }
}
