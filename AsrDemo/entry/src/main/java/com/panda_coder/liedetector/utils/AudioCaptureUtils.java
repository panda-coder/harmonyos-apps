package com.panda_coder.liedetector.utils;

import ohos.media.audio.AudioCapturer;
import ohos.media.audio.AudioCapturerInfo;
import ohos.media.audio.AudioStreamInfo;
import ohos.media.audio.SoundEffect;

import java.util.UUID;

public class AudioCaptureUtils {

    private AudioStreamInfo audioStreamInfo;
    private AudioCapturer audioCapturer;
    private AudioCapturerInfo audioCapturerInfo;

    public AudioCaptureUtils() {

    }

    public AudioCaptureUtils(AudioStreamInfo.ChannelMask channelMask, int SampleRate) {
        this.audioStreamInfo = new AudioStreamInfo.Builder()
                .encodingFormat(AudioStreamInfo.EncodingFormat.ENCODING_PCM_16BIT)
                .channelMask(channelMask)
                .sampleRate(SampleRate)
                .build();
        this.audioCapturerInfo = new AudioCapturerInfo.Builder().audioStreamInfo(audioStreamInfo).build();
    }

    public void setAudioStreamInfo(AudioStreamInfo audioStreamInfo) {
        this.audioStreamInfo = audioStreamInfo;
        this.audioCapturerInfo = new AudioCapturerInfo.Builder().audioStreamInfo(audioStreamInfo).build();
    }

    public void init(String packageName) {
        this.init(SoundEffect.SOUND_EFFECT_TYPE_NS,packageName );
    }

    public AudioCapturer get(){
        return this.audioCapturer;
    }

    public void stop(){
        this.audioCapturer.stop();
    }

    public void destory(){
        this.audioCapturer.stop();
        this.audioCapturer.release();
    }

    public Boolean start() {
        if (audioCapturer == null)
            return false;
        return audioCapturer.start();
    }

    public int read(byte[] buffers, int offset, int bytesLength){
        return audioCapturer.read(buffers,offset,bytesLength);
    }

    public void init(UUID soundEffect, String packageName) {
        if (audioCapturer == null || audioCapturer.getState() == AudioCapturer.State.STATE_UNINITIALIZED)
            audioCapturer = new AudioCapturer(this.audioCapturerInfo);
        audioCapturer.addSoundEffect(soundEffect, packageName);
    }
}
