package com.panda_coder.liedetector;

import com.panda_coder.liedetector.utils.AsrUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.rpc.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;

import java.util.HashMap;
import java.util.Map;

public class ControllerAbility extends Ability {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "Demo");
    AnswerRemote remote = new AnswerRemote();
    AsrUtils asrUtils;
    private static HashMap<Integer, IRemoteObject> remoteObjectHandlers = new HashMap<Integer, IRemoteObject>();

    @Override
    public void onStart(Intent intent) {
        HiLog.error(LABEL_LOG, "ControllerAbility::onStart");
        super.onStart(intent);
        asrUtils = new AsrUtils(this);
    }

    @Override
    public void onBackground() {
        super.onBackground();
        HiLog.info(LABEL_LOG, "ControllerAbility::onBackground");
    }

    @Override
    public void onStop() {
        super.onStop();
        HiLog.info(LABEL_LOG, "ControllerAbility::onStop");
    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        super.onConnect(intent);
        return remote.asObject();
    }

    class AnswerRemote extends RemoteObject implements IRemoteBroker {
        AnswerRemote() {
            super("");
        }

        @Override
        public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
            Map<String, Object> zsonResult = new HashMap<String, Object>();
            String zsonStr = data.readString();
            ZSONObject zson = ZSONObject.stringToZSON(zsonStr);
            switch (code) {
                case 100: {
                    asrUtils.start();
                    break;
                }
                case 101: {
                    asrUtils.stop();
                    break;
                }
                case 200: {
                    remoteObjectHandlers.put(200 ,data.readRemoteObject());
                    getAsrText();
                    break;
                }
                default: {
                    reply.writeString("service not defined");
                    return false;
                }
            }
            reply.writeString(ZSONObject.toZSONString(zsonResult));
            return true;
        }

        @Override
        public IRemoteObject asObject() {
            return this;
        }
    }

    public void getAsrText() {
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1 * 500);
                    Map<String, Object> zsonResult = new HashMap<String, Object>();
                    zsonResult.put("text",asrUtils.getResultAndClear());
                    ReportEvent(200, zsonResult);

                } catch (RemoteException | InterruptedException e) {
                    break;
                }
            }
        }).start();
    }

    private void ReportEvent(int remoteHandler, Object backData) throws RemoteException {
        MessageParcel data = MessageParcel.obtain();
        MessageParcel reply = MessageParcel.obtain();
        MessageOption option = new MessageOption();
        data.writeString(ZSONObject.toZSONString(backData));
        IRemoteObject remoteObject = remoteObjectHandlers.get(remoteHandler);
        remoteObject.sendRequest(100, data, reply, option);
        reply.reclaim();
        data.reclaim();
    }

    @Override
    public void onDisconnect(Intent intent) {
    }
}