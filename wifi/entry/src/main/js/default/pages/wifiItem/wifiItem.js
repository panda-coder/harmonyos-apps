import '../../common/StringExtend.js'

export default {
    props:["value",'checked'],
    computed:{
        ssid(){
            return (this.value && this.value.ssid.utf8ToStr()) || ''
        },
        rssi(){
            return "../../images/wifi-2.png";
        },
        desc(){
            let str="";
            let securityType= (this.value && this.value.securityType.toString()) || '';
            if(this.checked)
                securityType="1000";
            switch(securityType){
                case "0":
                    str="不安全的网络连接";break;
                case "1":
                    str="开放网络";break;
                case "2":
                    str="加密(WEP)";break;
                case "3":
                    str="加密(WPA-PSK)";break;
                case "4":
                    str="加密(WPA)";break;
                case "1000":
                    str="已连接";break;
                default:
                    str="可用的";break;
            }
            return str;
        }
    },
    boxTouchend(){
        this.$emit('myTouched',this.value)
    }
}
