export default {
    props:["value"],
    data: {
        numbers:"1234567890",
        letters:["qwertyuiop","asdfghjkl","zxcvbnm"],
        symbols:["#$%^&*_+<>","-/：；（）—@“”","…～、？！.，。"],
        isUp:false,
        isShowSymbol:false,
    },
    onInit(){
        this.value=this.value || "";
    },
    computed:{
        numberKeys(){
            return this.numbers.split('');
        },
        letterKeys(){
            let keys=[];
            this.letters.forEach(t=>{
                //大小写转换
                t=this.isUp?t.toUpperCase():t.toLowerCase();
                keys.push(t.split(''))
            })
            return keys;
        },
        symbolKeys(){
            let keys=[];
            this.symbols.forEach(t=>{
                keys.push(t.split(''))
            })
            return keys;
        }
    },
    upKey(){
         this.isUp=!this.isUp;
    },
    deleteOne(){
        this.value=this.value.slice(0,this.value.length-1);
        this._changed();
    },
    keyDown(key){
        this.value+=key;
        this._changed();
    },
    closeIt(){
        this.$emit("close")
    },
    _changed(){
        this.$emit('change',this.value)
    },
    showSymbol(){
        this.isShowSymbol=true;
    },
    showLetter(){
        this.isShowSymbol=false;
    }
}
