/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
//import wifi from '@ohos.wifi_native_js';
import prompt from '@system.prompt'
import '../../common/StringExtend.js'
export default {
    data: {
        wifiInfo:{
            enable:false,
            isActive:false,
            scanInfo:[{ssid:"123",securityType:0},{ssid:"234",securityType:1},{ssid:"234",securityType:1},{ssid:"234",securityType:1},{ssid:"234",securityType:1},{ssid:"234",securityType:1},{ssid:"234",securityType:1},{ssid:"234",securityType:1}]//开发调试用无效数据
        },
        wifiPassword:'',
        wifichecked:{},
        connectItem:{},
        isScaning:false,
        isKeyBoradShow:false,
        touchMovedList:[],
        console:""
    },
    computed:{
        state(){
            let str="当前未连接";
            if(!this.wifiInfo.isActive){
                str="请先打开WIFI"
            }else{
                if(this.connectItem && this.connectItem.ssid){
                    str=`已连接:${this.connectItem.ssid.utf8ToStr()}`
                }else{
                    str="当前未连接"
                }
            }
            return str;
        },
        WifiScanList(){
            let list=this.wifiInfo.scanInfo;
            if(Object.keys(this.connectItem).length<=0)
                return list;
            list=list.filter(t=>t.bssid!=this.connectItem.bssid);
            return list;
        },
        checkedWifiName(){
            if(this.wifichecked && this.wifichecked.ssid){
                return this.wifichecked.ssid.utf8ToStr();
            }
            return "";
        },
        isItemMoved(){
          return  this.touchMovedList.length>5;
        }
    },
    onInit() {
      //this.wifiInfo.isActive=wifi.isWifiActive();
      //this.wifiInfo.isActive&&this.wifiScaning();
    },
    //WiFi开启关闭按钮点击事件
    wifiSwitchChanged({checked}){
        this.wifiInfo.isActive=checked;
        if(checked){
            let wifistate=false;
          //  wifistate=wifi.enableWifi();
            if(!wifistate){
               prompt.showToast({
                   message:"wifi开启失败请重试",
                   duration:1500
               })
               this.wifiInfo.isActive=false;
               return;
            }
            this.wifiScaning();
        }else{
            this.wifiInfo.scanInfo=[];
            this.wifichecked={}
           // wifi.disableWifi();
        }
    },
    //下拉刷新WiFi列表
    refreshTouched({state}){
        if(state=="start" && this.wifiInfo.isActive)
            this.wifiScaning();
    },
    //扫描wifi
    wifiScaning:async  function(){
        this.isScaning=true;
       // wifi.scan();
//        await wifi.getScanInfos((result)=>{
//            let ss=JSON.parse(JSON.stringify(result));
//            prompt.showToast({
//                message:"获取到wifi扫描列表,当前共有"+ss.length+"个wifi",
//                duration:1500
//            })
//            this.wifiInfo.scanInfo=ss;
//            this.isScaning=false;
//        });
    },
    //选中wifi弹出输入密码框
    checkedWifi(data){
        this.wifichecked=data.detail;
        this.showInputPassword();
    },
    //连接按钮事件
    connect(){
        this.hideInputPassword()
        this.connectWifi();
    },
    //连接wifi
    connectWifi(){
        let WifiDeviceConfig={
            ssid:this.wifichecked.ssid,
            bssid:this.wifichecked.bssid,
            preSharedKey:this.wifiPassword,
            isHiddenSsid:false,
            securityType:this.wifichecked.securityType
        }
       // let status= wifi.connectToDevice(WifiDeviceConfig);
//        if(status){
//            this.connectItem=this.wifichecked
//        }else{
//            prompt.showToast({
//                message:"连接失败，请检查密码是否正确",
//                duration:1500
//            })
//        }
//        try{
//            wifi.getLinkedInfo(t=>{
//                prompt.showToast({
//                    message:JSON.stringify(t),
//                    duration:2500
//                })
//            }).catch(error=>{
//                console.error("获取dhcp linkInfo fail"+error);
//            })
//        }catch(e){
//            console.error("获取linkedInfo failed")
//        }
//        try{
//            wifi.getDhcpInfo(t=>{
//                prompt.showToast({
//                    message:JSON.stringify(t),
//                    duration:2500
//                })
//            }).catch(error=>{
//                console.error("获取dhcp fail"+error);
//            })
//        }catch(e){
//            console.error("获取dhcp fail")
//        }

    },
    //显示输入WiFi密码
    showInputPassword(){
        this.wifiPassword='';
        if(this.isItemMoved)
            return;
        this.$element("wifi_dialog").show();
    },
    //关闭输入WiFi密码
    hideInputPassword(){
        this.$element("wifi_dialog").close();
    },

    //列表下拉滚动事件，当滚动时阻止弹出显示WiFi密码
    itemMoved(evt){
        this.touchMovedList.push(1);
        setTimeout(()=>{this.touchMovedList=[]},1000)
    },
    //恢复状态
    listItemToucheStart(){
    }
}



