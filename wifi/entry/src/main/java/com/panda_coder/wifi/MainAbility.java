package com.panda_coder.wifi;

import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        requestPermissions();
    }

    private void requestPermissions() {
        if (verifySelfPermission(SystemPermission.LOCATION) != IBundleManager.PERMISSION_GRANTED) {
            requestPermissionsFromUser(new String[]{SystemPermission.LOCATION}, 0);
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions == null || permissions.length == 0 || grantResults == null || grantResults.length == 0) {
            return;
        }
        if (requestCode == 0) {
            if (grantResults[0] != IBundleManager.PERMISSION_GRANTED) {

                terminateAbility();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
