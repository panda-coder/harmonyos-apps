package com.panda_coder.amapcard.widget.widget;

import com.panda_coder.amapcard.db.AMapCardKVStore;
import com.panda_coder.amapcard.entity.MapInfo;
import com.panda_coder.amapcard.entity.WidgetInfo;
import com.panda_coder.amapcard.utils.HttpImageUtils;
import com.panda_coder.amapcard.utils.WidgetDBManager;
import com.panda_coder.amapcard.widget.controller.FormController;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.FormBindingData;
import ohos.aafwk.ability.ProviderFormInfo;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.location.Location;
import ohos.location.Locator;
import ohos.location.LocatorCallback;
import ohos.location.RequestParam;
import ohos.utils.zson.ZSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class WidgetImpl extends FormController {
    //高德地图授权的key
    private String AMapKey="";
    private WidgetInfo widgetInfo;
    private Boolean slocationChanged=false;//位置是否修改
    private static Locator locator=null;//坐标获取类
    private LocatorCallBack locatorCallBack=new LocatorCallBack();//坐标获取后返回调用类
    private List<String> mKeyLocation=new ArrayList<>();//静态地图获取周边标记的坐标
    RequestParam requestParam = new RequestParam(RequestParam.PRIORITY_ACCURACY, 20, 0);


    public WidgetImpl(Context context, String formName, Integer dimension,Long formId) {
        super(context, formName, dimension,formId);
        AMapCardKVStore.init(context);
        widgetInfo=new WidgetInfo(dimension);
        widgetInfo.setFormId(formId);
        if(locator==null){
            widgetInfo.setLocation(new Location(30.737149,104.144184));
           // locator=new Locator(context);
           // locator.startLocating(requestParam,locatorCallBack);
        }
        if(WidgetDBManager.getInstance(context).hasEntity(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId)))
            WidgetDBManager.getInstance(context).update(widgetInfo);
        else
            WidgetDBManager.getInstance(context).insert(widgetInfo);
    }

    @Override
    public void updateFormData(long formId, Object... vars) {
        widgetInfo=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId));
        ZSONObject zsonObject=new ZSONObject();
        zsonObject.put("searchBtns",widgetInfo.getButtonArray());
        zsonObject.put("searchText",widgetInfo.getButtonChecked());
        String mapName="amap"+System.currentTimeMillis()+".png";
        zsonObject.put("imgSrc","memory://"+mapName);
        FormBindingData bindingData = new FormBindingData(zsonObject);
        byte[] bytes=getImageBytes();
        bindingData.addImageData(mapName,bytes);
        try{
            ((Ability)context).updateForm(formId,bindingData);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void onTriggerFormEvent(long formId, String message) {
        widgetInfo=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId));
        ZSONObject request=ZSONObject.stringToZSON(message);
        String EventName=request.getString("name");
        switch (EventName){
            case "checkSearch":{
                int index=request.getIntValue("index");
                widgetInfo.setButtonChecked(index);
                break;
            }
            case "mapAdd":{
                widgetInfo.leve1Add(1);
                break;
            }
            case "mapReduce":{
                widgetInfo.levelReduce(1);
                break;
            }
        }
        WidgetDBManager.getInstance(context).update(widgetInfo);
        updateFormData(formId);
    }

    @Override
    public Class<? extends AbilitySlice> getRoutePageSlice(Intent intent) {
        return null;
    }

    @Override
    public ProviderFormInfo bindCreateFormData(){
        WidgetInfo info=new WidgetInfo(this.dimension);
        FormBindingData formBindingData=null;
        ZSONObject zsonObject =new ZSONObject();
        zsonObject.put("imgSrc","memory://amap.png");
        zsonObject.put("showCtlButton",this.dimension!=2);
        zsonObject.put("searchBtns",info.getButtonArray());
        zsonObject.put("searchText",info.getButtonChecked());

        formBindingData=new FormBindingData(zsonObject);
        ProviderFormInfo formInfo = new ProviderFormInfo();
        formInfo.setJsBindingData(formBindingData);
        byte[] bytes= getImageBytes();
        formBindingData.addImageData("amap.png",bytes);
        return formInfo;
    }

    private byte[] getImageBytes(){
        widgetInfo=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId));
        this.refreshMark();
        String otherParams=widgetInfo.getLevel()+"-"+widgetInfo.getMapSize()+"-"+widgetInfo.getLocation()+"-"+String.join(",",mKeyLocation);
        String imgKey="mapImg-"+md5(otherParams);
        MapInfo entity;
        //插入/更新值到数据库中，以便其它地方根据时间关系删除冗余数据
        try{
            if(WidgetDBManager.getInstance(context).hasEntity(WidgetDBManager.getInstance(context).where(MapInfo.class).equalTo("key",imgKey))){
                entity=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(MapInfo.class).equalTo("key",imgKey));
                entity.setUpdateDate(new Date());
                WidgetDBManager.getInstance(context).update(entity);
            }else{
                entity=new MapInfo(imgKey);
                WidgetDBManager.getInstance(context).insert(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        //从分布式数据库中查找是否存在该值
        byte[] result;
        if(AMapCardKVStore.singleKvStore.getEntries(imgKey).size()>0){
           result=AMapCardKVStore.singleKvStore.getByteArray(imgKey);
            return result;
        }
        String amapUrl=getMapImageUrl(mKeyLocation);
        result= HttpImageUtils.doGetRequestForFile(amapUrl);
        if(result.length>1024)
            AMapCardKVStore.singleKvStore.putByteArray(imgKey,result);
        return result;
    }


    private void refreshMark(){
        try{
            widgetInfo=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId));
            if(widgetInfo.getButtonChecked()=="未设置")
                return;
            String otherParams=widgetInfo.getLocation()+"-"+widgetInfo.getButtonChecked();
            String markKey="markKey-"+md5(otherParams);
            MapInfo entity;
            //插入/更新值到数据库中，以便其它地方根据时间关系删除冗余数据
            try{
                if(WidgetDBManager.getInstance(context).hasEntity(WidgetDBManager.getInstance(context).where(MapInfo.class).equalTo("key",markKey))){
                    entity=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(MapInfo.class).equalTo("key",markKey));
                    entity.setUpdateDate(new Date());
                    WidgetDBManager.getInstance(context).update(entity);
                }else{
                    entity=new MapInfo(markKey);
                    WidgetDBManager.getInstance(context).insert(entity);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
            //从分布式数据库中查找是否存在该值
             if(AMapCardKVStore.singleKvStore.getEntries(markKey).size()>0){
                String marks=AMapCardKVStore.singleKvStore.getString(markKey);
                 mKeyLocation= Arrays.asList(marks.split("\\|"));
                 return;
            }
            this.mKeyLocation= HttpImageUtils.SearchByKeyUrl(getMapMarkUrl(10));
            if(this.mKeyLocation.size()>0 && this.mKeyLocation.get(0)!="")
                AMapCardKVStore.singleKvStore.putString(markKey,String.join("|",this.mKeyLocation));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private String getMapImageUrl(List<String> Position){
        widgetInfo=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId));
        String url="https://restapi.amap.com/v3/staticmap";
        String params="key="+this.AMapKey;
        params+="&zoom="+widgetInfo.getLevel();
        params+="&size="+widgetInfo.getMapSize();
        if(widgetInfo.getRLocation()!=null)
            params+="&location="+widgetInfo.getLocation();
        params+="&markers=large,0xea7700,H:"+widgetInfo.getLocation();
        if(Position==null || Position.size()==0 || Position.get(0)=="")
            return  url+"?"+params;
        params+=("|mid,0xFF0000,:"+String.join(";",Position));
        return url+"?"+params;
    }

    private  String getMapMarkUrl(int size){
        widgetInfo=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId));
        String Url="https://restapi.amap.com/v5/place/around?";
        Url+="key="+this.AMapKey;
        Url+="&keywords="+widgetInfo.getButtonChecked();
        if(widgetInfo.getRLocation()!=null)
            Url+="&location="+widgetInfo.getLocation();
        Url+="&size="+size;
        return Url;
    }

    public class LocatorCallBack implements LocatorCallback{

        @Override
        public void onLocationReport(Location location) {
            widgetInfo=WidgetDBManager.getInstance(context).getFirst(WidgetDBManager.getInstance(context).where(WidgetInfo.class).equalTo("formId",formId));
            if((location.getLongitude()+","+location.getLatitude())==widgetInfo.getLocation())
                return;
            widgetInfo.setLocation(location);
            updateFormData(formId);
        }

        @Override
        public void onStatusChanged(int i) {

        }

        @Override
        public void onErrorReport(int i) {

        }
    }

    private String md5(String str){
        String result=null;
        try {
            // 加密对象，指定加密方式
            MessageDigest md5 = MessageDigest.getInstance("md5");
            // 准备要加密的数据
            byte[] b = str.getBytes();
            // 加密
            byte[] digest = md5.digest(b);
            // 十六进制的字符
            char[] chars = new char[] { '0', '1', '2', '3', '4', '5',
                    '6', '7' , '8', '9', 'A', 'B', 'C', 'D', 'E','F' };
            StringBuffer sb = new StringBuffer();
            // 处理成十六进制的字符串(通常)
            for (byte bb : digest) {
                sb.append(chars[(bb >> 4) & 15]);
                sb.append(chars[bb & 15]);
            }
            result=sb.toString();
            // 打印加密后的字符串
            System.out.println(sb);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }
}
