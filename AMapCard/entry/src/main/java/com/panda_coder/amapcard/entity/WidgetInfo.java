package com.panda_coder.amapcard.entity;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.*;
import ohos.location.Location;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName="widget_info",ignoredColumns = {"buttonArray","mapSize"})
public class WidgetInfo extends OrmObject {
    public WidgetInfo(){

    }
    public WidgetInfo(int dimension){
        this.dimension=dimension;
        switch (dimension){
            case 2:{
                this.level=12;
                break;
            }
            case 3:{
                this.level=13;
                break;
            }
            case 4:{
                this.level=15;
                break;
            }
        }
        this.setButtonStr("酒店|餐饮|景点|加油站");
        this.buttonChecked=this.getButtonArray().get(0);
    }

    @PrimaryKey(autoGenerate =false)
    private long formId;
    private String buttonStr;
    private int dimension;
    private String location;
    private int level;
    private String mapSize;
    private String buttonChecked;

    public String getButtonStr(){
        return this.buttonStr;
    }
    public void setButtonStr(String buttonStr){
        this.buttonStr=buttonStr;
    }


    public List<String> getButtonArray(){
        String[] buttons=this.buttonStr.split("\\|");
        List<String> result=new ArrayList<>();
       for(int i=0;i< 5;i++){
           if(buttons.length>i){
               result.add(buttons[i]);
           }else{
               result.add("未设置");
           }
       }
       return result;
    }

    public int getDimension(){
       return this.dimension;
    }
    public void  setDimension(int dimension){
        this.dimension=dimension;
    }

    public long getFormId(){
        return this.formId;
    }
    public void setFormId(long formId){
        this.formId=formId;
    }

    public String getLocation(){
        return this.location;
    }
    public Location getRLocation(){
        String[] locations=this.location.split(",");
        if(locations.length<=0)
            return null;
        return new Location(Double.parseDouble(locations[1]),Double.parseDouble(locations[0]));
    }

    public void setLocation(String location){
        this.location=location;
    }

    public void setLocation(Location location){
        this.location=location.getLongitude()+","+location.getLatitude();
    }

    public int getLevel(){
        return this.level;
    }
    public void setLevel(int level){
        this.level=level;
    }
    public void leve1Add(int Number){
        if(this.level<17)
            this.level+=Number;
    }
    public void levelReduce(int Number){
        if(this.level>1)
          this.level-=Number;
    }

    public String getMapSize(){
        switch (this.dimension){
           case 2:
                return "250*250";
           case 3:
                return "500*250";
           case 4:
                return "500*500";
            default:
                return "250*250";
        }
    }

    public String getButtonChecked(){
        return this.buttonChecked;
    }

    public void  setButtonChecked(String mark){
        this.buttonChecked=mark;
    }
    public void setButtonChecked(int i){
        if(i>getButtonArray().size())
            i=getButtonArray().size();
        this.buttonChecked=this.getButtonArray().get(i);
    }
}
