package com.panda_coder.amapcard.db;

import com.panda_coder.amapcard.entity.*;
import ohos.data.orm.OrmDatabase;
import ohos.data.orm.annotation.Database;

@Database(entities={WidgetInfo.class,MapInfo.class},version=1)
public abstract class AMapCardStore extends OrmDatabase {

}
