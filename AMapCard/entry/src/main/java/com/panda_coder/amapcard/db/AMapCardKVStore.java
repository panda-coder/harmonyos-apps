package com.panda_coder.amapcard.db;

import ohos.app.Context;
import ohos.data.distributed.common.*;
import ohos.data.distributed.user.SingleKvStore;

public class AMapCardKVStore {
    static KvManagerConfig config=null;
    static KvManager manager=null;
    public static SingleKvStore singleKvStore=null ;
    static String storeId="AMapCardApp";

  public static void  init(Context context){
      if(config==null)
        config=new KvManagerConfig(context);
      if(manager==null)
        manager= KvManagerFactory.getInstance().createKvManager(config);
      Options options=new Options();
      if(singleKvStore==null){
          try{
              options.setCreateIfMissing(true).setEncrypt(false).setKvStoreType(KvStoreType.SINGLE_VERSION).setAutoSync(true);
              singleKvStore =manager.getKvStore(options,storeId);
          }catch (KvStoreException ex){
              ex.printStackTrace();
          }
      }
  }

}
