package com.panda_coder.amapcard.utils;

import com.panda_coder.amapcard.db.AMapCardStore;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;

public  class WidgetDBHelper {
    protected DatabaseHelper helper=null;
    protected OrmContext ormContext=null;
    public WidgetDBHelper(Context context){
        DatabaseHelper helper = new DatabaseHelper(context);
        ormContext= helper.getOrmContext("WidgetStore","WidgetStore.db", AMapCardStore.class);
    }

}
