package com.panda_coder.amapcard.utils;

import ohos.app.Context;
import ohos.data.orm.OrmObject;
import ohos.data.orm.OrmPredicates;

import java.util.List;

public class WidgetDBManager extends WidgetDBHelper {
    private static WidgetDBManager instance=null;

    public  WidgetDBManager(Context context){
        super(context);
    }

    public static WidgetDBManager getInstance(Context context){
        if(instance==null){
           instance= new WidgetDBManager(context);
        }
        return instance;
    }

    public <T extends OrmObject> boolean insert(T info){
        boolean result=false;
        result =instance.ormContext.insert(info);
         this.flush();
         return result;
    }

    public void beginTransaction(){
       instance.ormContext.beginTransaction();
    }
    public void commit(){
        instance.ormContext.commit();
    }
    public void rollback(){
        instance.ormContext.rollback();
    }

    public <T extends OrmObject>  boolean update(T entity){
        boolean result=false;
        result= instance.ormContext.update(entity);
        this.flush();
        return result;
    }
    public boolean flush(){
       return  instance.ormContext.flush();
    }


    public <T extends  OrmObject> boolean delete(T entity){
       boolean result=false;
       result= instance.ormContext.delete(entity);
       this.flush();
        return result;
    }

    public <T extends OrmObject> List<T> query(OrmPredicates predicates){
        return instance.ormContext.query(predicates);
    }

    public <T extends  OrmObject> T getFirst(OrmPredicates predicates){
        T entity=null;
        List<T> result=this.query(predicates);
        if(result.size()>0){
            entity=result.get(0);
        }
        return entity;
    }

    public <T extends OrmObject> OrmPredicates where(Class<T> c){
        return instance.ormContext.where(c);
    }

    public <T extends OrmObject> boolean hasEntity(OrmPredicates predicates){
       return this.query(predicates).size()>0;
    }

}

