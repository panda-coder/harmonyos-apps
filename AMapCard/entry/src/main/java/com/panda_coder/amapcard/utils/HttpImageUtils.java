package com.panda_coder.amapcard.utils;

import com.panda_coder.amapcard.MainAbility;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HttpImageUtils {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.DEBUG, 0x0, MainAbility.class.getName());

    public final static byte[] doGetRequestForFile(String urlStr) {
        InputStream is = null;
        HttpURLConnection conn = null;
        byte[] buff = new byte[1024];
        try {
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();

            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setReadTimeout(6000);
            conn.connect();
            is = conn.getInputStream();
            if (conn.getResponseCode() == 200) {
                buff = readInputStream(is);
            } else{
                buff=null;
            }
        } catch (Exception e) {
            HiLog.error(TAG,"【获取图片异常】",e);
        }
        finally {
            try {
                if(is != null){
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            conn.disconnect();
        }

        return buff;
    }

    public static byte[] readInputStream(InputStream is) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = -1;
        try {
            while ((length = is.read(buffer)) != -1) {
                baos.write(buffer, 0, length);
            }
            baos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] data = baos.toByteArray();
        try {
            is.close();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static String httpGet(String urlStr){
        InputStream is = null;
        HttpURLConnection conn = null;
        String response="";
        StringBuffer buffer = new StringBuffer();
        try {
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();

            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setReadTimeout(6000);
            conn.connect();
            is = conn.getInputStream();
            if (conn.getResponseCode() == 200) {
                String str=null;
                InputStreamReader isr = new InputStreamReader(is,"utf-8");
                BufferedReader br = new BufferedReader(isr);
                while((response = br.readLine())!=null){
                    buffer.append(response);
                }
            }
            response=buffer.toString();

        } catch (Exception e) {
            HiLog.error(TAG,"【访问异常】",e);
        }
        finally {
            try {
                if(is != null){
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            conn.disconnect();
        }
        return response;
    }

    public final  static List<String> SearchByKeyUrl(String urlStr){
        List<String> result=new ArrayList<>();
        String response= httpGet(urlStr);
        if(response==null || response=="")
            return result;
        ZSONObject zson=ZSONObject.stringToZSON(response);
        if(zson.getIntValue("infocode")!=10000)
            return result;
        ZSONArray zsonArray=zson.getZSONArray("pois");
        for(int i=0;i<zsonArray.size();i++){
            ZSONObject child= (ZSONObject)zsonArray.get(i);
            String location=child.getString("location");
            result.add(location);
        }
        return result;
    }
}
