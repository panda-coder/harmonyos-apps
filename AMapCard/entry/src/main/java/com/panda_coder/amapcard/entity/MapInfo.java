package com.panda_coder.amapcard.entity;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

import java.util.Date;

@Entity(tableName="map_info",ignoredColumns = {})
public class MapInfo extends OrmObject {
    @PrimaryKey(autoGenerate = false)
    private String key;
    private Date updateDate;

    public MapInfo() {
        super();
    }

    public  MapInfo(String key){
        this.setKey(key);
        this.setUpdateDate(new Date());
    }
    public Date getUpdateDate(){
        return this.updateDate;
    }
    public void setUpdateDate(Date updateDate){
        this.updateDate=updateDate;
    }
    public String getKey(){
        return this.key;
    }
    public void setKey(String key){
        this.key=key;
    }
}
