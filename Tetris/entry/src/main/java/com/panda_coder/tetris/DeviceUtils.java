package com.panda_coder.tetris;


import ohos.account.AccountAbility;
import ohos.account.DistributedInfo;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;

import java.util.ArrayList;
import java.util.List;

public class DeviceUtils {
    private static final String TAG = DeviceUtils.class.getSimpleName();

    private DeviceUtils() {}

    /**
     * Get group id.
     * @return group id.
     */
    public static String getGroupId() {
        AccountAbility account = AccountAbility.getAccountAbility();
        DistributedInfo distributeInfo = account.queryOsAccountDistributedInfo();
        return distributeInfo.getId();
    }

    public static List<String> getAvailableDeviceId() {
        List<String> deviceIds = new ArrayList<>();

        List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
        if (deviceInfoList == null) {
            return deviceIds;
        }

        if (deviceInfoList.size() == 0) {
            System.out.println("did not find other device");
            return deviceIds;
        }

        for (DeviceInfo deviceInfo : deviceInfoList) {
            deviceIds.add(deviceInfo.getDeviceId());
        }

        return deviceIds;
    }

    /**
     * Get available id
     * @return available device ids
     */
    public static List<String> getAllAvailableDeviceId() {
        List<String> deviceIds = new ArrayList<>();

        List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ALL_DEVICE);
        if (deviceInfoList == null) {
            return deviceIds;
        }
        System.out.println("deviceInfoList size " + deviceInfoList.size());
        if (deviceInfoList.size() == 0) {
            System.out.println("did not find other device");
            return deviceIds;
        }

        for (DeviceInfo deviceInfo : deviceInfoList) {
            deviceIds.add(deviceInfo.getDeviceId());
        }

        return deviceIds;
    }

    /**
     * Get remote device info
     * @return Remote device info list.
     */
    public static List<DeviceInfo> getRemoteDevice() {
        List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
        return deviceInfoList;
    }

    /**
     * @Param []
     * @description  获取当前组网下可迁移的设备id
     * @return java.lang.String
     */
    private String getDeviceId(){
        String deviceId = "";
        List<String> outerDevices = DeviceUtils.getAvailableDeviceId();
        System.out.println("getDeviceId DeviceUtils.getRemoteDevice() = " + outerDevices);
        if (outerDevices == null || outerDevices.size() == 0){
            System.out.println( "no other device to continue");
        }else {
            for (String item : outerDevices) {
                System.out.println( "item deviceId = " + item);
            }
            deviceId = outerDevices.get(0);
        }
        System.out.println("continueAbility to deviceId = " + deviceId);
        return deviceId;
    };
}
