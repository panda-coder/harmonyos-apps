package com.panda_coder.tetris;

import ohos.aafwk.content.Intent;
import ohos.ace.ability.AceAbility;
import ohos.agp.window.service.WindowManager;
import ohos.bundle.AbilityInfo;

public class ControllerAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        setInstanceName("controller");
        this.getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);
        super.onStart(intent);
        this.setDisplayOrientation(AbilityInfo.DisplayOrientation.LANDSCAPE);
    }


    @Override
    public void onStop() {
        super.onStop();
    }
}
