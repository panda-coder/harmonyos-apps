package com.panda_coder.tetris;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.bundle.ElementName;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.rpc.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControllerRemoteAbility extends Ability  {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "Demo");
    private static boolean isConnected=false;
    private static String  selectedDeviceId="";
    private static int controllerCmd=-1;
    private static final int SUB_CMD_MODEL=1002;
    ControllerInterfaceProxy controllerInterfaceProxy=null;
    private static HashMap<Integer,IRemoteObject> remoteObjectHandlers=new HashMap<Integer, IRemoteObject>();
    private JSRemote jsRemote = new JSRemote("jsRemote");
    private ControllerRemoteObject controllerRemoteObject=new ControllerRemoteObject("");
    @Override
    public void onStart(Intent intent) {
        HiLog.error(LABEL_LOG, "ControllerRemoteAbility::onStart");
        super.onStart(intent);
    }

    @Override
    public void onBackground() {
        super.onBackground();
        HiLog.info(LABEL_LOG, "ControllerRemoteAbility::onBackground");
    }

    @Override
    public void onStop() {
        super.onStop();
        HiLog.info(LABEL_LOG, "ControllerRemoteAbility::onStop");
    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        super.onConnect(intent);
        IRemoteObject rob=null;
        intent.getOperation().getDeviceId();
        if(intent.getFlags()==0){
            rob= jsRemote.asObject();

        }else if(intent.getFlags()==Intent.FLAG_ABILITYSLICE_MULTI_DEVICE) {
            rob= controllerRemoteObject.asObject();
        }
        return rob;
    }

    @Override
    public void onDisconnect(Intent intent) {
        isConnected=false;
    }

    private class ControllerRemoteObject extends ControllerInterfaceStub{
        public ControllerRemoteObject(String descriptor){
            super(descriptor);
        }

        @Override
        public void touchedController(int cmd) throws RemoteException {
            controllerCmd=cmd;
        }
    }

    private  class JSRemote extends RemoteObject implements IRemoteBroker{

        public JSRemote(String descriptor) {
            super(descriptor);
        }

        @Override
        public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) throws RemoteException {
            // 返回结果仅支持可序列化的Object类型
            String s= data.readString();
            ZSONObject request= ZSONObject.stringToZSON(s);
            Map<String, Object> zsonResult = new HashMap<String, Object>();
            Response responses=new Response();
            switch (code){
                //获取分布式设备
                case 1001:{
                    List<DeviceInfo> devices= DeviceUtils.getRemoteDevice();
                    List<Response.DeviceInfo> result=new ArrayList<>();
                    for(DeviceInfo item : devices){
                        Response.DeviceInfo entity=responses.new DeviceInfo();
                        entity.Id=item.getDeviceId();
                        entity.Name=item.getDeviceName();
                        result.add(entity);
                    }
                    zsonResult.put("code",200);
                    zsonResult.put("devices",result);
                    break;
                }
                //订阅控制按钮
                case 1002:{
                    remoteObjectHandlers.put(SUB_CMD_MODEL ,data.readRemoteObject());
                    getDeviceCmd();
                    zsonResult.put("code",200);
                    break;
                }
                case 2001:{
                    int cmd=request.getIntValue("cmd");
                    if(controllerInterfaceProxy!=null){
                        controllerInterfaceProxy.touchedController(cmd);
                        zsonResult.put("code",200);
                    }else{
                        controllerCmd=cmd;
                    }
                    break;
                }
                case 2002:{
                    String deviceId=request.getString("deviceId");
                    selectedDeviceId=deviceId;
                    if(connectRemoteAbility()){
                        startMainAility();
                       // controllerInterfaceProxy.touchedController(100);
                        zsonResult.put("code",200);
                    }else{
                        zsonResult.put("code",-100);
                        zsonResult.put("msg","连接失败");
                    }
                    break;
                }
                default: {
                    reply.writeString("service not defined");
                    return false;
                }
            }
            reply.writeString(ZSONObject.toZSONString(zsonResult));
            return true;
        }
        @Override
        public IRemoteObject asObject() {
            return this;
        }
    }

    public  void  getDeviceCmd(){
        new Thread(()->{
            while (true){
                try{
                    Thread.sleep(1 * 100);
                    if(controllerCmd!=-1){
                        Map<String,Object> zsonResult=new HashMap<String,Object>();
                        zsonResult.put("cmd",controllerCmd);
                        ReportEvent(SUB_CMD_MODEL,zsonResult);
                    }
                    controllerCmd=-1;

                }catch (RemoteException | InterruptedException e) {
                    break;
                }
            }
        }).start();
    }

    private void ReportEvent(int remoteHandler,Object backData) throws RemoteException {
        MessageParcel data = MessageParcel.obtain();
        MessageParcel reply = MessageParcel.obtain();
        MessageOption option = new MessageOption();
        data.writeString(ZSONObject.toZSONString(backData));
        IRemoteObject remoteObject =remoteObjectHandlers.get(remoteHandler);
        remoteObject.sendRequest(100,data,reply,option);
        reply.reclaim();
        data.reclaim();
    }

    private  void startMainAility() {
        Intent mainIntent = getRemoteServiceIntent("com.panda_coder.tetris", "MainAbility");
        startAbility(mainIntent);
    }

    private boolean connectRemoteAbility(){
        boolean isRemote=false;
        Intent remotePageIntent = getRemoteServiceIntent("com.panda_coder.tetris", "ControllerRemoteAbility");
        startAbility(remotePageIntent);
        try{
           isRemote= connectAbility(remotePageIntent,remoteAbilityConnection);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return isRemote;
    }

    private Intent getRemoteServiceIntent(String bundleName, String serviceName) {
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId(selectedDeviceId)
                .withBundleName(bundleName)
                .withAbilityName(serviceName)
                .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                .build();
        Intent intent = new Intent();
        intent.setOperation(operation);
        return intent;
    }

    private IAbilityConnection remoteAbilityConnection = new IAbilityConnection() {
        @Override
        public void onAbilityConnectDone(ElementName elementName, IRemoteObject iRemoteObject, int i) {
            controllerInterfaceProxy = new ControllerInterfaceProxy(iRemoteObject);
        }

        @Override
        public void onAbilityDisconnectDone(ElementName elementName, int i) {
            controllerInterfaceProxy = null;
        }
    };

}