package com.panda_coder.tetris;

import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;
import ohos.bundle.AbilityInfo;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.rpc.*;
import ohos.utils.zson.ZSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        requestPermission();
    }

    //获取权限
    private void requestPermission() {
        String[] permission = {
                "ohos.permission.DISTRIBUTED_DATASYNC",
        };
        List<String> applyPermissions = new ArrayList<>();
        for (String element : permission) {
            if (verifySelfPermission(element) != 0) {
                if (canRequestPermission(element)) {
                    applyPermissions.add(element);
                }
            }
        }
        requestPermissionsFromUser(applyPermissions.toArray(new String[0]), 0);
    }


    @Override
    public void onStop() {
        super.onStop();
    }
}
