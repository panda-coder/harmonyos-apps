export const CONTROLLERUP=1;
export const CONTROLLERRIGHT=2;
export const CONTROLLERDOWN=3;
export const CONTROLLERLEFT=4;
export const CONTROLLERSTART=10;
export const CONTROLLERRESTART=20;
export const CONTROLLERCENTER=99;

const triangle_1=[
    [0,0,0,0],
    [0,0,0,0],
    [0,1,0,0],
    [1,1,1,0]
]
const triangle_2=[
    [0,0,0,0],
    [0,1,0,0],
    [1,1,0,0],
    [0,1,0,0]
]
const triangle_3=[
    [0,0,0,0],
    [0,0,0,0],
    [1,1,1,0],
    [0,1,0,0]
]
const triangle_4=[
    [0,0,0,0],
    [1,0,0,0],
    [1,1,0,0],
    [1,0,0,0]
];

const matts=[
    [0,0,0,0],
    [0,0,0,0],
    [1,1,0,0],
    [1,1,0,0]
]

const article_1=[
    [1,0,0,0],
    [1,0,0,0],
    [1,0,0,0],
    [1,0,0,0],
]

const article_2=[
    [0,0,0,0],
    [0,0,0,0],
    [0,0,0,0],
    [1,1,1,1]
]
const z_1=[
    [0,0,0,0],
    [1,0,0,0],
    [1,1,0,0],
    [0,1,0,0]
]
const z_2=[
    [0,0,0,0],
    [0,0,0,0],
    [0,1,1,0],
    [1,1,0,0]
]
const RZ_1=[
    [0,0,0,0],
    [0,1,0,0],
    [1,1,0,0],
    [1,0,0,0]
]
const RZ_2=[
    [0,0,0,0],
    [0,0,0,0],
    [1,1,0,0],
    [0,1,1,0]
]

const L_1=[
    [0,0,0,0],
    [1,1,0,0],
    [0,1,0,0],
    [0,1,0,0]
]
const L_2=[
    [0,0,0,0],
    [0,0,0,0],
    [1,1,1,0],
    [1,0,0,0]
]
const L_3=[
    [0,0,0,0],
    [1,0,0,0],
    [1,0,0,0],
    [1,1,0,0]
]
const L_4=[
    [0,0,0,0],
    [0,0,0,0],
    [0,0,1,0],
    [1,1,1,0]
]

const RL_1=[
    [0,0,0,0],
    [1,1,0,0],
    [1,0,0,0],
    [1,0,0,0]
]
const RL_2=[
    [0,0,0,0],
    [0,0,0,0],
    [1,0,0,0],
    [1,1,1,0]
]
const RL_3=[
    [0,0,0,0],
    [0,1,0,0],
    [0,1,0,0],
    [1,1,0,0]
]
const RL_4=[
    [0,0,0,0],
    [0,0,0,0],
    [1,1,1,0],
    [0,0,1,0]
]

export const model={
    triangle:[triangle_1,triangle_2,triangle_3,triangle_4],
    matts:[matts],
    article:[article_1,article_2],
    z:[z_1,z_2],
    rz:[RZ_1,RZ_2],
    L:[L_1,L_2,L_3,L_4],
    RL:[RL_1,RL_2,RL_3,RL_4]
}

export const groundX=8;
export const groundY=16;
export const speed=300;
export const min=function(a,b){
    let result=a[0][b];
    a.forEach(t=>{
        result= t[b]>result?result:t[b]
    })
    return result;
}
export const max=function(a,b){
    let result=a[0][b];
    a.forEach(t=>{
        result= t[b]>result?t[b]:result
    })
    return result;
}
export const groupBy=function(array, name) {
  const groups = {}
  array.forEach(function (o) {
    const group = JSON.stringify(o[name])
    groups[group] = groups[group] || []
    groups[group].push(o)
  })
  return groups;
}