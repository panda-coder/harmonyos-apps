
const ABILITY_TYPE_EXTERNAL = 0;
const ABILITY_TYPE_INTERNAL = 1;
const ACTION_SYNC = 0;
const BUNDLENAME="com.panda_coder.tetris";


export  const jsCallJavaAbility={
    callAbility: async function(abilityName,messageCode, data,abilityType){
        abilityType=abilityType || ABILITY_TYPE_EXTERNAL
        let action = {};
        action.bundleName = BUNDLENAME;
        action.abilityName = abilityName;
        action.messageCode = messageCode || 1001;
        action.data = data;
        action.abilityType = abilityType;
        action.syncOption = ACTION_SYNC;
        let result= await FeatureAbility.callAbility(action);
        return JSON.parse(result);
    },
//根据messeage_code 订阅不同事件
    subAbility: async function(abilityName,messageCode,callBack,abilityType){
        let action={};
        abilityType=abilityType || ABILITY_TYPE_EXTERNAL
        action.bundleName =BUNDLENAME;
        action.abilityName = abilityName;
        action.messageCode = messageCode;
        action.abilityType = abilityType;
        action.syncOption = ACTION_SYNC;
        let result = await FeatureAbility.subscribeAbilityEvent(action, function(callbackData) {
            var callbackJson = JSON.parse(callbackData);
            this.eventData = JSON.stringify(callbackJson.data);
            callBack && callBack(callbackJson);
        });
        return JSON.parse(result);
    },
//根据message_code取消订阅不同事件
    unSubAbility : async function(abilityName,messageCode,abilityType){
        let action={};
        abilityType=abilityType || ABILITY_TYPE_EXTERNAL
        action.bundleName = BUNDLENAME;
        action.abilityName = abilityName;
        action.messageCode = messageCode;
        action.abilityType = abilityType;
        action.syncOption = ACTION_SYNC;
        let result= await FeatureAbility.unsubscribeAbilityEvent(action);
        return JSON.parse(result);
    }
}