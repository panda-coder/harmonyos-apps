import {CONTROLLERUP,CONTROLLERCENTER,CONTROLLERRIGHT,CONTROLLERDOWN,CONTROLLERLEFT,CONTROLLERSTART,CONTROLLERRESTART} from "../../../Common/construct.js"
import {jsCallJavaAbility} from '../../../Common/JsCallJavaAbilityUtils.js'
import prompt from '@system.prompt'

export default {
    data:{
        isRemote:false,
        refresh:false,
        checkedIndex:-1,
        deviceInfos:[]
    },
    ctrTouched(data,evt){
        let result=-1;
        switch(data){
            case "up":
            result=CONTROLLERUP;
            break;
            case "right":
            result=CONTROLLERRIGHT;break;
            case "down":
            result=CONTROLLERDOWN;break;
            case "left":
            result=CONTROLLERLEFT;break;
            case "center":
            result=CONTROLLERCENTER;break;
            case "start":
            result=CONTROLLERSTART;break;
            case "restart":
            result=CONTROLLERRESTART;break;
            case "showCtl":
            result=100;break;
        }
        jsCallJavaAbility.callAbility("ControllerRemoteAbility",2001,{cmd:result}).then(result=>{
            if(result.code==200){
                console.log("send successed");
            }
        })
    },
//流转
    remoteIt(){
        if(this.checkedIndex==-1){
            prompt.showToast({
                message:"请先选择设备",
                duration:1500
            })
            return;
        }
        jsCallJavaAbility.callAbility("ControllerRemoteAbility",2002,{deviceId:this.deviceInfos[this.checkedIndex].Id}).then(result=>{
            if(result.code==200){
                this.closeDeviceDialog();
                let target = {
                    bundleName: "com.panda_coder.tetris",
                    abilityName: "ControllerAbility",
                    data: {
                        isRemote:true
                    }
                };
                FeatureAbility.startAbility(target);
            }else{
                prompt.showToast({
                    message:"流转失败",
                    duration:1500
                })
            }
        })
    },
    //连接Java PA获取设备信息
    getDeviceInfos(){
        this.refresh=true;
        jsCallJavaAbility.callAbility("ControllerRemoteAbility",1001,null).then(result=>{
            if(result.code==200){
                this.deviceInfos=result.devices;
            }
            this.refresh=false;
        });
    },
    showDeviceDialog(){
        this.getDeviceInfos();
        this.$element("deviceInfo").show();
    },
    closeDeviceDialog(){
        this.$element("deviceInfo").close();
    },
    checkedDevice(data,index){
        this.checkedIndex=index;
    }

}
