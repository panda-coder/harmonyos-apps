/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.hml?entry");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./lib/json.js!./lib/style.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.css":
/*!**********************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/style.js!G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index.css ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  ".container": {
    "display": "flex",
    "justifyContent": "center",
    "alignItems": "center",
    "alignContent": "center"
  },
  ".box-left": {
    "width": "65%",
    "justifyContent": "center",
    "flexDirection": "column",
    "alignContent": "center"
  },
  ".box-right": {
    "width": "35%",
    "display": "flex",
    "flexDirection": "column",
    "justifyContent": "center",
    "alignItems": "center"
  },
  ".box-right text": {
    "fontSize": "15px"
  },
  ".ctr-vertical": {
    "justifyContent": "space-around",
    "height": "96%"
  },
  ".ctr-horizontal": {
    "justifyContent": "space-around",
    "width": "100%"
  },
  ".box-right button": {
    "width": "90px",
    "height": "90px"
  },
  "button": {
    "fontSize": "30px",
    "fontWeight": "bolder",
    "height": "42%",
    "width": "42%",
    "backgroundColor": "#4b4bac",
    "boxShadowH": "3px",
    "boxShadowV": "2px",
    "boxShadowBlur": "2px",
    "boxShadowColor": "#4e4e4e",
    "marginTop": "5%",
    "marginRight": "12%",
    "marginBottom": "5%",
    "marginLeft": "12%"
  },
  ".top-button": {
    "backgroundColor": "#d5a27b",
    "textColor": "#FFFFFF",
    "marginTop": "3px",
    "marginRight": "20px",
    "marginBottom": "3px",
    "marginLeft": "20px"
  },
  "#deviceInfo": {
    "bottom": "0px",
    "width": "100%",
    "height": "300px"
  },
  "#deviceInfo list": {
    "paddingTop": "10px",
    "paddingRight": "10px",
    "paddingBottom": "10px",
    "paddingLeft": "10px"
  },
  ".device-list": {
    "display": "flex",
    "borderBottomWidth": "1px",
    "borderBottomStyle": "solid",
    "borderBottomColor": "#cccccc",
    "paddingTop": "3px",
    "paddingRight": "10px",
    "paddingBottom": "3px",
    "paddingLeft": "10px",
    "justifyContent": "space-between",
    "alignContent": "space-between",
    "alignItems": "center"
  },
  ".device-list text": {
    "fontSize": "20px"
  },
  ".tools": {
    "justifyContent": "center",
    "alignContent": "center",
    "alignItems": "center"
  },
  ".tools button": {
    "fontSize": "16px",
    "width": "100px",
    "backgroundColor": "#168cef"
  }
}

/***/ }),

/***/ "./lib/json.js!./lib/style.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.css":
/*!*******************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/style.js!G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/default/pages/index/index.css ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  ".container": {
    "display": "flex",
    "flexDirection": "column",
    "justifyContent": "center",
    "width": "100%",
    "height": "100%",
    "backgroundColor": "#d5a27b"
  },
  ".div-top": {
    "display": "flex",
    "width": "100%"
  },
  ".div-bottom": {
    "display": "flex",
    "width": "100%",
    "height": "300px"
  },
  ".content": {
    "width": "70%",
    "height": "100%",
    "backgroundColor": "#8e8e8e",
    "borderTopWidth": "1px",
    "borderRightWidth": "1px",
    "borderBottomWidth": "1px",
    "borderLeftWidth": "1px",
    "borderTopStyle": "solid",
    "borderRightStyle": "solid",
    "borderBottomStyle": "solid",
    "borderLeftStyle": "solid",
    "borderTopColor": "#cccccc",
    "borderRightColor": "#cccccc",
    "borderBottomColor": "#cccccc",
    "borderLeftColor": "#cccccc"
  },
  ".content-position": {
    "backgroundColor": "#eaeaea",
    "borderTopWidth": "1px",
    "borderRightWidth": "1px",
    "borderBottomWidth": "1px",
    "borderLeftWidth": "1px",
    "borderTopStyle": "solid",
    "borderRightStyle": "solid",
    "borderBottomStyle": "solid",
    "borderLeftStyle": "solid",
    "borderTopColor": "#a6cccccc",
    "borderRightColor": "#a6cccccc",
    "borderBottomColor": "#a6cccccc",
    "borderLeftColor": "#a6cccccc",
    "width": "200px",
    "height": "200px"
  },
  ".score": {
    "paddingTop": "30px",
    "paddingRight": "0px",
    "paddingBottom": "30px",
    "paddingLeft": "0px",
    "width": "30%",
    "height": "100%",
    "display": "flex",
    "flexDirection": "column",
    "justifyContent": "space-between"
  },
  ".score text": {
    "fontSize": "16px",
    "color": "#FFFFFF",
    "marginTop": "3px",
    "marginRight": "3px",
    "marginBottom": "3px",
    "marginLeft": "3px"
  },
  ".score>div": {
    "display": "flex",
    "width": "100%",
    "flexDirection": "column",
    "justifyContent": "center",
    "alignItems": "center"
  },
  ".next": {
    "display": "grid",
    "height": "80px",
    "width": "80px",
    "gridTemplateColumns": "1fr 1fr 1fr 1fr",
    "gridTemplateRows": "1fr 1fr 1fr 1fr"
  },
  ".next .isChecked": {
    "borderTopWidth": "1px",
    "borderRightWidth": "1px",
    "borderBottomWidth": "1px",
    "borderLeftWidth": "1px",
    "borderTopStyle": "solid",
    "borderRightStyle": "solid",
    "borderBottomStyle": "solid",
    "borderLeftStyle": "solid",
    "borderTopColor": "#cccccc",
    "borderRightColor": "#cccccc",
    "borderBottomColor": "#cccccc",
    "borderLeftColor": "#cccccc",
    "width": "20px",
    "height": "20px",
    "backgroundColor": "#31313b"
  },
  ".btn": {
    "width": "80px",
    "borderBottomLeftRadius": "6px",
    "borderBottomRightRadius": "6px",
    "borderTopLeftRadius": "6px",
    "borderTopRightRadius": "6px",
    "paddingTop": "6px",
    "paddingRight": "3px",
    "paddingBottom": "6px",
    "paddingLeft": "3px",
    "textColor": "#ffffff",
    "backgroundColor": "#d5a27b",
    "marginTop": "3px",
    "marginRight": "3px",
    "marginBottom": "3px",
    "marginLeft": "3px"
  },
  ".isChecked": {
    "backgroundColor": "#31313b"
  },
  ".next .unchecked": {
    "backgroundColor": "#d5a27b",
    "borderTopWidth": "0px",
    "borderRightWidth": "0px",
    "borderBottomWidth": "0px",
    "borderLeftWidth": "0px"
  }
}

/***/ }),

/***/ "./lib/json.js!./lib/template.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.hml":
/*!*************************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/template.js!G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index.hml ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  "attr": {
    "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:1"
  },
  "type": "div",
  "style": {
    "flexDirection": "column",
    "backgroundColor": "#d5a27b"
  },
  "children": [
    {
      "attr": {
        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:2",
        "className": "container"
      },
      "type": "div",
      "classList": [
        "container"
      ],
      "children": [
        {
          "attr": {
            "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:3",
            "type": "button",
            "className": "top-button",
            "value": "开 始"
          },
          "type": "input",
          "onBubbleEvents": {
            "touchend": function (evt) {this.ctrTouched('start',evt)}
          },
          "classList": [
            "top-button"
          ]
        },
        {
          "attr": {
            "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:4",
            "type": "button",
            "className": "top-button",
            "value": "重新开始"
          },
          "type": "input",
          "onBubbleEvents": {
            "touchend": function (evt) {this.ctrTouched('restart',evt)}
          },
          "classList": [
            "top-button"
          ]
        },
        {
          "attr": {
            "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:5",
            "type": "button",
            "show": function () {return !this.isRemote},
            "className": "top-button",
            "value": "流 转"
          },
          "type": "input",
          "onBubbleEvents": {
            "touchend": "showDeviceDialog"
          },
          "classList": [
            "top-button"
          ]
        },
        {
          "attr": {
            "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:6",
            "type": "button",
            "show": function () {return this.isRemote},
            "className": "top-button",
            "value": "对方控制"
          },
          "type": "input",
          "onBubbleEvents": {
            "touchend": function (evt) {this.ctrTouched('showCtl',evt)}
          },
          "classList": [
            "top-button"
          ]
        }
      ]
    },
    {
      "attr": {
        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:8",
        "className": "container"
      },
      "type": "div",
      "classList": [
        "container"
      ],
      "children": [
        {
          "attr": {
            "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:9",
            "className": "box-left"
          },
          "type": "div",
          "classList": [
            "box-left"
          ],
          "children": [
            {
              "attr": {
                "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:10"
              },
              "type": "stack",
              "style": {
                "justifyContent": "center",
                "alignItems": "center"
              },
              "children": [
                {
                  "attr": {
                    "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:12",
                    "className": "ctr-vertical"
                  },
                  "type": "div",
                  "style": {
                    "flexDirection": "column"
                  },
                  "classList": [
                    "ctr-vertical"
                  ],
                  "children": [
                    {
                      "attr": {
                        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:13",
                        "value": "↑"
                      },
                      "type": "button",
                      "onBubbleEvents": {
                        "touchend": function (evt) {this.ctrTouched('up',evt)}
                      }
                    },
                    {
                      "attr": {
                        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:14",
                        "value": "↓"
                      },
                      "type": "button",
                      "onBubbleEvents": {
                        "touchend": function (evt) {this.ctrTouched('down',evt)}
                      }
                    }
                  ]
                },
                {
                  "attr": {
                    "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:17",
                    "className": "ctr-horizontal"
                  },
                  "type": "div",
                  "style": {
                    "flexDirection": "row"
                  },
                  "classList": [
                    "ctr-horizontal"
                  ],
                  "children": [
                    {
                      "attr": {
                        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:18",
                        "value": "←"
                      },
                      "type": "button",
                      "onBubbleEvents": {
                        "touchend": function (evt) {this.ctrTouched('left',evt)}
                      }
                    },
                    {
                      "attr": {
                        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:19",
                        "value": "→"
                      },
                      "type": "button",
                      "onBubbleEvents": {
                        "touchend": function (evt) {this.ctrTouched('right',evt)}
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "attr": {
            "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:23",
            "className": "box-right"
          },
          "type": "div",
          "classList": [
            "box-right"
          ],
          "children": [
            {
              "attr": {
                "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:24",
                "type": "circle",
                "className": "ctr-center"
              },
              "type": "button",
              "classList": [
                "ctr-center"
              ],
              "onBubbleEvents": {
                "touchend": function (evt) {this.ctrTouched('center',evt)}
              }
            }
          ]
        }
      ]
    },
    {
      "attr": {
        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:27",
        "id": "deviceInfo"
      },
      "type": "dialog",
      "id": "deviceInfo",
      "children": [
        {
          "attr": {
            "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:28",
            "refreshing": function () {return this.refresh},
            "type": "auto"
          },
          "type": "refresh",
          "events": {
            "refresh": "getDeviceInfos"
          },
          "children": [
            {
              "attr": {
                "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:29"
              },
              "type": "list",
              "children": [
                {
                  "attr": {
                    "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:30",
                    "className": "device-list"
                  },
                  "type": "list-item",
                  "classList": [
                    "device-list"
                  ],
                  "repeat": function () {return this.deviceInfos},
                  "onBubbleEvents": {
                    "touchend": function (evt) {this.checkedDevice(this.$item,this.$idx,evt)}
                  },
                  "children": [
                    {
                      "attr": {
                        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:31",
                        "value": function () {return this.$item.Name}
                      },
                      "type": "text"
                    },
                    {
                      "attr": {
                        "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:32",
                        "value": function () {return this.$idx},
                        "type": "radio",
                        "checked": function () {return this.$idx==this.checkedIndex}
                      },
                      "type": "input"
                    }
                  ]
                }
              ]
            },
            {
              "attr": {
                "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:35",
                "className": "tools"
              },
              "type": "div",
              "classList": [
                "tools"
              ],
              "children": [
                {
                  "attr": {
                    "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:36",
                    "value": "流 转"
                  },
                  "type": "button",
                  "onBubbleEvents": {
                    "touchend": "remoteIt"
                  }
                },
                {
                  "attr": {
                    "debugLine": "G:gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index:37",
                    "value": "取 消"
                  },
                  "type": "button",
                  "onBubbleEvents": {
                    "touchend": "closeDeviceDialog"
                  }
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}

/***/ }),

/***/ "./lib/json.js!./lib/template.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.hml":
/*!**********************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/template.js!G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/default/pages/index/index.hml ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  "attr": {
    "debugLine": "pages/index/index:3",
    "className": "container"
  },
  "type": "div",
  "classList": [
    "container"
  ],
  "children": [
    {
      "attr": {
        "debugLine": "pages/index/index:4",
        "className": "div-top"
      },
      "type": "div",
      "classList": [
        "div-top"
      ],
      "children": [
        {
          "attr": {
            "debugLine": "pages/index/index:5"
          },
          "type": "stack",
          "style": {
            "justifyContent": "center",
            "alignItems": "center"
          },
          "children": [
            {
              "attr": {
                "debugLine": "pages/index/index:6"
              },
              "type": "div",
              "children": [
                {
                  "attr": {
                    "debugLine": "pages/index/index:8",
                    "className": "content"
                  },
                  "type": "div",
                  "classList": [
                    "content"
                  ],
                  "style": {
                    "display": "grid",
                    "gridTemplateColumns": function () {return this.grid_x},
                    "gridTemplateRows": function () {return this.grid_y}
                  },
                  "children": [
                    {
                      "attr": {
                        "debugLine": "pages/index/index:9",
                        "className": "content-position {{$item.value==1?'isChecked':'unchecked'}}"
                      },
                      "type": "div",
                      "repeat": function () {return this.positions},
                      "classList": function () {return ['content-position', this.$item.value==1?'isChecked':'unchecked']}
                    }
                  ]
                },
                {
                  "attr": {
                    "debugLine": "pages/index/index:13",
                    "className": "score"
                  },
                  "type": "div",
                  "classList": [
                    "score"
                  ],
                  "children": [
                    {
                      "attr": {
                        "debugLine": "pages/index/index:14"
                      },
                      "type": "div",
                      "children": [
                        {
                          "attr": {
                            "debugLine": "pages/index/index:15",
                            "value": "当前分数:"
                          },
                          "type": "text"
                        },
                        {
                          "attr": {
                            "debugLine": "pages/index/index:16",
                            "value": function () {return this.score}
                          },
                          "type": "text"
                        },
                        {
                          "attr": {
                            "debugLine": "pages/index/index:17",
                            "value": function () {return this.console}
                          },
                          "type": "text"
                        }
                      ]
                    },
                    {
                      "attr": {
                        "debugLine": "pages/index/index:19"
                      },
                      "type": "div",
                      "children": [
                        {
                          "attr": {
                            "debugLine": "pages/index/index:20",
                            "value": "下一个"
                          },
                          "type": "text"
                        },
                        {
                          "attr": {
                            "debugLine": "pages/index/index:21",
                            "className": "next"
                          },
                          "type": "div",
                          "classList": [
                            "next"
                          ],
                          "style": {
                            "display": "grid"
                          },
                          "children": [
                            {
                              "attr": {
                                "debugLine": "pages/index/index:22",
                                "className": "{{$item.value==1?'isChecked':'unchecked'}}"
                              },
                              "type": "div",
                              "repeat": function () {return this.nextModel},
                              "classList": function () {return [this.$item.value==1?'isChecked':'unchecked']},
                              "style": {
                                "display": "grid"
                              }
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "attr": {
                "debugLine": "pages/index/index:27"
              },
              "type": "div",
              "style": {
                "justifyContent": "center",
                "paddingTop": "6px",
                "paddingRight": "30px",
                "paddingBottom": "6px",
                "paddingLeft": "30px",
                "marginTop": "0px",
                "marginRight": "10px",
                "marginBottom": "0px",
                "marginLeft": "10px",
                "backgroundColor": "#A9A9A9",
                "borderBottomLeftRadius": "6px",
                "borderBottomRightRadius": "6px",
                "borderTopLeftRadius": "6px",
                "borderTopRightRadius": "6px",
                "boxShadowH": "1px",
                "boxShadowV": "1px",
                "boxShadowBlur": "3px",
                "boxShadowColor": "#cccccc"
              },
              "shown": function () {return this.isGameOver},
              "children": [
                {
                  "attr": {
                    "debugLine": "pages/index/index:28",
                    "value": "游戏结束"
                  },
                  "type": "text",
                  "style": {
                    "color": "#ce0f0f"
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "attr": {
        "debugLine": "pages/index/index:33",
        "className": "div-bottom",
        "show": function () {return this.isShowController}
      },
      "type": "div",
      "classList": [
        "div-bottom"
      ],
      "children": [
        {
          "attr": {
            "debugLine": "pages/index/index:34"
          },
          "type": "ctr",
          "events": {
            "ctr-touchend": "ctrTouchend"
          }
        }
      ]
    }
  ]
}

/***/ }),

/***/ "./lib/loader.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.hml?name=ctr":
/*!******************************************************************************************************************!*\
  !*** ./lib/loader.js!G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index.hml?name=ctr ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $app_template$ = __webpack_require__(/*! !./lib/json.js!./lib/template.js!./index.hml */ "./lib/json.js!./lib/template.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.hml")
var $app_style$ = __webpack_require__(/*! !./lib/json.js!./lib/style.js!./index.css */ "./lib/json.js!./lib/style.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.css")
var $app_script$ = __webpack_require__(/*! !./lib/script.js!./node_modules/babel-loader?presets[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!./index.js */ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.js")

$app_define$('@app-component/ctr', [], function($app_require$, $app_exports$, $app_module$) {

$app_script$($app_module$, $app_exports$, $app_require$)
if ($app_exports$.__esModule && $app_exports$.default) {
$app_module$.exports = $app_exports$.default
}

$app_module$.exports.template = $app_template$

$app_module$.exports.style = $app_style$

})


/***/ }),

/***/ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.js":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./lib/script.js!./node_modules/babel-loader/lib?presets[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/controller/pages/index/index.js ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = function(module, exports, $app_require$){"use strict";

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _newArrowCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/newArrowCheck */ "./node_modules/@babel/runtime/helpers/newArrowCheck.js"));

var _construct = __webpack_require__(/*! ../../../Common/construct.js */ "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\Common\\construct.js");

var _JsCallJavaAbilityUtils = __webpack_require__(/*! ../../../Common/JsCallJavaAbilityUtils.js */ "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\Common\\JsCallJavaAbilityUtils.js");

var _system = _interopRequireDefault($app_require$("@app-module/system.prompt"));

var _default = {
  data: {
    isRemote: false,
    refresh: false,
    checkedIndex: -1,
    deviceInfos: []
  },
  ctrTouched: function ctrTouched(data, evt) {
    var _this = this;

    var result = -1;

    switch (data) {
      case "up":
        result = _construct.CONTROLLERUP;
        break;

      case "right":
        result = _construct.CONTROLLERRIGHT;
        break;

      case "down":
        result = _construct.CONTROLLERDOWN;
        break;

      case "left":
        result = _construct.CONTROLLERLEFT;
        break;

      case "center":
        result = _construct.CONTROLLERCENTER;
        break;

      case "start":
        result = _construct.CONTROLLERSTART;
        break;

      case "restart":
        result = _construct.CONTROLLERRESTART;
        break;

      case "showCtl":
        result = 100;
        break;
    }

    _JsCallJavaAbilityUtils.jsCallJavaAbility.callAbility("ControllerRemoteAbility", 2001, {
      cmd: result
    }).then(function (result) {
      (0, _newArrowCheck2["default"])(this, _this);

      if (result.code == 200) {
        console.log("send successed");
      }
    }.bind(this));
  },
  remoteIt: function remoteIt() {
    var _this2 = this;

    if (this.checkedIndex == -1) {
      _system["default"].showToast({
        message: "请先选择设备",
        duration: 1500
      });

      return;
    }

    _JsCallJavaAbilityUtils.jsCallJavaAbility.callAbility("ControllerRemoteAbility", 2002, {
      deviceId: this.deviceInfos[this.checkedIndex].Id
    }).then(function (result) {
      (0, _newArrowCheck2["default"])(this, _this2);

      if (result.code == 200) {
        this.closeDeviceDialog();
        var target = {
          bundleName: "com.panda_coder.tetris",
          abilityName: "ControllerAbility",
          data: {
            isRemote: true
          }
        };
        FeatureAbility.startAbility(target);
      } else {
        _system["default"].showToast({
          message: "流转失败",
          duration: 1500
        });
      }
    }.bind(this));
  },
  getDeviceInfos: function getDeviceInfos() {
    var _this3 = this;

    this.refresh = true;

    _JsCallJavaAbilityUtils.jsCallJavaAbility.callAbility("ControllerRemoteAbility", 1001, null).then(function (result) {
      (0, _newArrowCheck2["default"])(this, _this3);

      if (result.code == 200) {
        this.deviceInfos = result.devices;
      }

      this.refresh = false;
    }.bind(this));
  },
  showDeviceDialog: function showDeviceDialog() {
    this.getDeviceInfos();
    this.$element("deviceInfo").show();
  },
  closeDeviceDialog: function closeDeviceDialog() {
    this.$element("deviceInfo").close();
  },
  checkedDevice: function checkedDevice(data, index) {
    this.checkedIndex = index;
  }
};
exports["default"] = _default;
var moduleOwn = exports.default || module.exports;
var accessors = ['public', 'protected', 'private'];
if (moduleOwn.data && accessors.some(function (acc) {
    return moduleOwn[acc];
  })) {
  throw new Error('For VM objects, attribute data must not coexist with public, protected, or private. Please replace data with public.');
} else if (!moduleOwn.data) {
  moduleOwn.data = {};
  moduleOwn._descriptor = {};
  accessors.forEach(function(acc) {
    var accType = typeof moduleOwn[acc];
    if (accType === 'object') {
      moduleOwn.data = Object.assign(moduleOwn.data, moduleOwn[acc]);
      for (var name in moduleOwn[acc]) {
        moduleOwn._descriptor[name] = {access : acc};
      }
    } else if (accType === 'function') {
      console.warn('For VM objects, attribute ' + acc + ' value must not be a function. Change the value to an object.');
    }
  });
}}
/* generated by ace-loader */


/***/ }),

/***/ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.js":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./lib/script.js!./node_modules/babel-loader/lib?presets[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/default/pages/index/index.js ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = function(module, exports, $app_require$){"use strict";

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _typeof2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/typeof */ "./node_modules/@babel/runtime/helpers/typeof.js"));

var _newArrowCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/newArrowCheck */ "./node_modules/@babel/runtime/helpers/newArrowCheck.js"));

var _construct = __webpack_require__(/*! ../../../Common/construct.js */ "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\Common\\construct.js");

var _JsCallJavaAbilityUtils = __webpack_require__(/*! ../../../Common/JsCallJavaAbilityUtils.js */ "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\Common\\JsCallJavaAbilityUtils.js");

var _default = {
  data: {
    offsetX: 0,
    offsetY: -3,
    modelChildIndex: -1,
    timeOut: null,
    isShowController: false,
    valueModel: Object.values(_construct.model),
    score: 0,
    modelIndex: -1,
    nextModelInfo: {},
    nextModel: [],
    positionResult: [],
    modelCount: Object.keys(_construct.model).length,
    isStart: false,
    console: ''
  },
  shareData: {
    ctrTouched: -1
  },
  computed: {
    isGameOver: function isGameOver() {
      var _this = this;

      var num = this.positionResult.filter(function (t) {
        (0, _newArrowCheck2["default"])(this, _this);
        return t.y <= 0;
      }.bind(this)).length;
      return num > 0;
    },
    positions: function positions() {
      var _this2 = this;

      var result = [];

      var _loop = function _loop(z) {
        var _loop2 = function _loop2(i) {
          var _this3 = this;

          result.push({
            positionX: i,
            positionY: z,
            value: _this2.positionResult.filter(function (t) {
              (0, _newArrowCheck2["default"])(this, _this3);
              return t.x == i && t.y == z;
            }.bind(this)).length > 0 || _this2.tempModel.filter(function (t) {
              (0, _newArrowCheck2["default"])(this, _this3);
              return t.x == i && t.y == z;
            }.bind(this)).length > 0 ? 1 : 0
          });
        };

        for (var i = 0; i < _construct.groundX; i++) {
          _loop2(i);
        }
      };

      for (var z = 0; z < _construct.groundY; z++) {
        _loop(z);
      }

      return result;
    },
    grid_x: function grid_x() {
      var result = "";

      for (var i = 0; i < _construct.groundX; i++) {
        result += " 1fr";
      }

      return result;
    },
    grid_y: function grid_y() {
      var result = "";

      for (var i = 0; i < _construct.groundY; i++) {
        result += " 1fr";
      }

      return result;
    },
    tempModel: function tempModel() {
      var _this4 = this;

      var result = [];
      if (this.modelIndex < 0) return [];
      var positionX = _construct.groundX / 2 - 1 + this.offsetX;
      var positionY = this.offsetY;
      var temp = this.getModelType(this.modelIndex, this.modelChildIndex);
      if (!temp) return [];
      var tmp = [];
      temp.forEach(function (t, a) {
        var _this5 = this;

        (0, _newArrowCheck2["default"])(this, _this4);
        t.forEach(function (x, b) {
          (0, _newArrowCheck2["default"])(this, _this5);
          tmp.push({
            positionX: b,
            positionY: a,
            value: x
          });
        }.bind(this));
      }.bind(this));
      tmp.forEach(function (t) {
        (0, _newArrowCheck2["default"])(this, _this4);

        if (t.value == 1) {
          result.push({
            x: t.positionX + positionX,
            y: t.positionY + positionY
          });
        }
      }.bind(this));
      return result;
    }
  },
  onInit: function onInit() {
    this.$watch('isGameOver', 'watchedGameOver');
    this.subCmd();
  },
  subCmd: function subCmd() {
    var _this6 = this;

    _JsCallJavaAbilityUtils.jsCallJavaAbility.subAbility("ControllerRemoteAbility", 1002, function (data) {
      (0, _newArrowCheck2["default"])(this, _this6);
      var cmd = data.data.cmd;
      this.ctrTouchend(cmd);
    }.bind(this)).then(function (result) {
      (0, _newArrowCheck2["default"])(this, _this6);

      if (result.code == 200) {
        console.log("订阅成功");
      }
    }.bind(this));
  },
  watchedGameOver: function watchedGameOver(newval, oldval) {
    if (newval) {
      clearInterval(this.timeOut);
    }
  },
  watchedPositionResult: function watchedPositionResult() {
    var _this7 = this;

    var isChanged = false;
    var tempData = JSON.parse(JSON.stringify(this.positionResult));
    var resultY = (0, _construct.groupBy)(tempData, "y");
    Object.values(resultY).forEach(function (t) {
      var _this8 = this;

      (0, _newArrowCheck2["default"])(this, _this7);

      if (t.length >= _construct.groundX) {
        isChanged = true;
        var positionY = t[0].y;
        t.map(function (x) {
          var _this9 = this;

          (0, _newArrowCheck2["default"])(this, _this8);
          var i = tempData.findIndex(function (a) {
            (0, _newArrowCheck2["default"])(this, _this9);
            return a == x;
          }.bind(this));
          tempData.splice(i, 1);
        }.bind(this));
        this.score += 100;
        tempData.filter(function (x) {
          (0, _newArrowCheck2["default"])(this, _this8);
          return x.y < positionY;
        }.bind(this)).map(function (x) {
          (0, _newArrowCheck2["default"])(this, _this8);
          return x.y += 1;
        }.bind(this));
      }
    }.bind(this));
    isChanged && (this.positionResult = tempData);
  },
  start: function start() {
    if (this.isStart) return;
    clearInterval(this.timeOut);
    this.timeOut = this.goNext();
    this.isStart = true;
    this.getNewModel();
  },
  restart: function restart() {
    this.score = 0;
    this.positionResult = [];
    this.isStart = true;
    clearInterval(this.timeOut);
    this.timeOut = this.goNext();
    this.getNewModel();
  },
  getNewModel: function getNewModel() {
    if (this.isGameOver) {
      clearInterval(this.timeOut);
      return;
    }

    this.offsetY = -3;
    this.offsetX = 0;

    if (JSON.stringify(this.nextModelInfo) != "{}") {
      this.modelIndex = this.nextModelInfo.index;
      this.modelChildIndex = this.nextModelInfo.childIndex;
    } else {
      var index = parseInt(Math.random() * this.modelCount);
      var childNum = Object.values(_construct.model)[index].length;
      this.modelChildIndex = parseInt(Math.random() * childNum);
      this.modelIndex = index;
    }

    this.getNextModel();
  },
  goNext: function goNext() {
    var _this10 = this;

    return setInterval(function (t) {
      (0, _newArrowCheck2["default"])(this, _this10);

      if (!this.checkGoNext(0, 1)) {
        this.positionResult = this.positionResult.concat(this.tempModel);
        this.watchedPositionResult();
        this.getNewModel();
      } else {
        this.moveDown();
      }
    }.bind(this), _construct.speed);
  },
  getModelType: function getModelType(index, childIndex) {
    return this.valueModel[index][childIndex];
  },
  getNextModel: function getNextModel() {
    var _this11 = this;

    var index = parseInt(Math.random() * this.modelCount);
    var childNum = this.valueModel[index].length;
    var childIndex = parseInt(Math.random() * childNum);
    var models = this.getModelType(index, childIndex);
    this.nextModelInfo = {
      index: index,
      childIndex: childIndex,
      models: models
    };
    this.nextModel = [];
    models.forEach(function (t, a) {
      var _this12 = this;

      (0, _newArrowCheck2["default"])(this, _this11);
      t.forEach(function (x, b) {
        (0, _newArrowCheck2["default"])(this, _this12);
        this.nextModel.push({
          positionX: b,
          positionY: a,
          value: x
        });
      }.bind(this));
    }.bind(this));
  },
  moveLeft: function moveLeft() {
    if (this.isGoNext(_construct.CONTROLLERLEFT)) this.offsetX -= 1;
  },
  moveRight: function moveRight() {
    if (this.isGoNext(_construct.CONTROLLERRIGHT)) this.offsetX += 1;
  },
  moveDown: function moveDown() {
    if (this.isGoNext(_construct.CONTROLLERDOWN)) this.offsetY += 1;
  },
  changeModel: function changeModel() {
    if (this.isGoNext(_construct.CONTROLLERCENTER)) {
      var models = this.valueModel[this.modelIndex];
      this.modelChildIndex = models.length - 1 > this.modelChildIndex ? this.modelChildIndex + 1 : 0;
    }

    this.continueAbility();
  },
  isGoNext: function isGoNext(cmd) {
    var result = false;

    switch (cmd) {
      case _construct.CONTROLLERRIGHT:
        result = this.checkGoNext(1, 0);
        break;

      case _construct.CONTROLLERLEFT:
        result = this.checkGoNext(-1, 0);
        break;

      case _construct.CONTROLLERDOWN:
        result = this.checkGoNext(0, 1);
        break;

      case _construct.CONTROLLERCENTER:
        result = this.checkChangeModel();
        break;
    }

    return result;
  },
  checkGoNext: function checkGoNext(x, y, model) {
    var _this13 = this;

    var result = true;
    model = model || this.tempModel;
    if (model.filter(function (t) {
      (0, _newArrowCheck2["default"])(this, _this13);
      return t.x + x < 0 || t.x + x >= _construct.groundX;
    }.bind(this)).length > 0) return result = false;
    if (model.filter(function (t) {
      (0, _newArrowCheck2["default"])(this, _this13);
      return t.y + y >= _construct.groundY;
    }.bind(this)).length > 0) return result = false;
    var position = {
      minX: (0, _construct.min)(this.tempModel, "x") + x,
      maxX: (0, _construct.max)(this.tempModel, "x") + x,
      minY: (0, _construct.min)(this.tempModel, "y") + y,
      maxY: (0, _construct.max)(this.tempModel, "y") + y
    };
    var data = this.positionResult.filter(function (t) {
      (0, _newArrowCheck2["default"])(this, _this13);
      return t.x >= position.minX && t.x <= position.maxX && t.y >= position.minY && t.y <= position.maxY;
    }.bind(this));

    if (data.length > 0) {
      var _loop3 = function _loop3(i) {
        var _this14 = this;

        var length = _this13.tempModel.filter(function (a) {
          (0, _newArrowCheck2["default"])(this, _this14);
          return a.x + x == data[i].x && a.y + y == data[i].y;
        }.bind(this)).length;

        if (length > 0) return {
          v: result = false
        };
      };

      for (var i = 0; i < data.length; i++) {
        var _ret = _loop3(i);

        if ((0, _typeof2["default"])(_ret) === "object") return _ret.v;
      }
    }

    return result;
  },
  checkChangeModel: function checkChangeModel() {
    var _this15 = this;

    var models = this.valueModel[this.modelIndex];
    var index = models.length - 1 > this.modelChildIndex ? this.modelChildIndex + 1 : 0;
    var nextTypeModel = models[index];
    var nextModelPosition = [];
    var nextModel = [];
    var positionX = _construct.groundX / 2 - 1 + this.offsetX;
    var positionY = 0 + this.offsetY;
    nextTypeModel.forEach(function (t, a) {
      var _this16 = this;

      (0, _newArrowCheck2["default"])(this, _this15);
      t.forEach(function (x, b) {
        (0, _newArrowCheck2["default"])(this, _this16);
        nextModel.push({
          positionX: b,
          positionY: a,
          value: x
        });
      }.bind(this));
    }.bind(this));
    nextModel.forEach(function (t) {
      (0, _newArrowCheck2["default"])(this, _this15);

      if (t.value == 1) {
        nextModelPosition.push({
          x: t.positionX + positionX,
          y: t.positionY + positionY
        });
      }
    }.bind(this));
    return this.checkGoNext(0, 0, nextModelPosition);
  },
  ctrTouchend: function ctrTouchend(cmd) {
    var data = cmd;

    switch (data) {
      case _construct.CONTROLLERRIGHT:
        this.moveRight();
        break;

      case _construct.CONTROLLERLEFT:
        this.moveLeft();
        break;

      case _construct.CONTROLLERDOWN:
        this.moveDown();
        break;

      case _construct.CONTROLLERCENTER:
        this.changeModel();
        break;

      case _construct.CONTROLLERSTART:
        this.start();
        break;

      case _construct.CONTROLLERRESTART:
        this.restart();
        break;

      case 100:
        {
          this.isShowController = !this.isShowController;
          break;
        }
    }
  }
};
exports["default"] = _default;
var moduleOwn = exports.default || module.exports;
var accessors = ['public', 'protected', 'private'];
if (moduleOwn.data && accessors.some(function (acc) {
    return moduleOwn[acc];
  })) {
  throw new Error('For VM objects, attribute data must not coexist with public, protected, or private. Please replace data with public.');
} else if (!moduleOwn.data) {
  moduleOwn.data = {};
  moduleOwn._descriptor = {};
  accessors.forEach(function(acc) {
    var accType = typeof moduleOwn[acc];
    if (accType === 'object') {
      moduleOwn.data = Object.assign(moduleOwn.data, moduleOwn[acc]);
      for (var name in moduleOwn[acc]) {
        moduleOwn._descriptor[name] = {access : acc};
      }
    } else if (accType === 'function') {
      console.warn('For VM objects, attribute ' + acc + ' value must not be a function. Change the value to an object.');
    }
  });
}}
/* generated by ace-loader */


/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/asyncToGenerator.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

module.exports = _asyncToGenerator;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/newArrowCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/newArrowCheck.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _newArrowCheck(innerThis, boundThis) {
  if (innerThis !== boundThis) {
    throw new TypeError("Cannot instantiate an arrow function");
  }
}

module.exports = _newArrowCheck;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/typeof.js":
/*!*******************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/typeof.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\Common\\JsCallJavaAbilityUtils.js":
/*!*****************************************************************************************!*\
  !*** G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/Common/JsCallJavaAbilityUtils.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jsCallJavaAbility = void 0;

var _regenerator = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js"));

var _asyncToGenerator2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js"));

var ABILITY_TYPE_EXTERNAL = 0;
var ABILITY_TYPE_INTERNAL = 1;
var ACTION_SYNC = 0;
var BUNDLENAME = "com.panda_coder.tetris";
var jsCallJavaAbility = {
  callAbility: function () {
    var _callAbility = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(abilityName, messageCode, data, abilityType) {
      var action, result;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              abilityType = abilityType || ABILITY_TYPE_EXTERNAL;
              action = {};
              action.bundleName = BUNDLENAME;
              action.abilityName = abilityName;
              action.messageCode = messageCode || 1001;
              action.data = data;
              action.abilityType = abilityType;
              action.syncOption = ACTION_SYNC;
              _context.next = 10;
              return FeatureAbility.callAbility(action);

            case 10:
              result = _context.sent;
              return _context.abrupt("return", JSON.parse(result));

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function callAbility(_x, _x2, _x3, _x4) {
      return _callAbility.apply(this, arguments);
    }

    return callAbility;
  }(),
  //根据messeage_code 订阅不同事件
  subAbility: function () {
    var _subAbility = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(abilityName, messageCode, callBack, abilityType) {
      var action, result;
      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              action = {};
              abilityType = abilityType || ABILITY_TYPE_EXTERNAL;
              action.bundleName = BUNDLENAME;
              action.abilityName = abilityName;
              action.messageCode = messageCode;
              action.abilityType = abilityType;
              action.syncOption = ACTION_SYNC;
              _context2.next = 9;
              return FeatureAbility.subscribeAbilityEvent(action, function (callbackData) {
                var callbackJson = JSON.parse(callbackData);
                this.eventData = JSON.stringify(callbackJson.data);
                callBack && callBack(callbackJson);
              });

            case 9:
              result = _context2.sent;
              return _context2.abrupt("return", JSON.parse(result));

            case 11:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function subAbility(_x5, _x6, _x7, _x8) {
      return _subAbility.apply(this, arguments);
    }

    return subAbility;
  }(),
  //根据message_code取消订阅不同事件
  unSubAbility: function () {
    var _unSubAbility = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(abilityName, messageCode, abilityType) {
      var action, result;
      return _regenerator["default"].wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              action = {};
              abilityType = abilityType || ABILITY_TYPE_EXTERNAL;
              action.bundleName = BUNDLENAME;
              action.abilityName = abilityName;
              action.messageCode = messageCode;
              action.abilityType = abilityType;
              action.syncOption = ACTION_SYNC;
              _context3.next = 9;
              return FeatureAbility.unsubscribeAbilityEvent(action);

            case 9:
              result = _context3.sent;
              return _context3.abrupt("return", JSON.parse(result));

            case 11:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    function unSubAbility(_x9, _x10, _x11) {
      return _unSubAbility.apply(this, arguments);
    }

    return unSubAbility;
  }()
};
exports.jsCallJavaAbility = jsCallJavaAbility;

/***/ }),

/***/ "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\Common\\construct.js":
/*!****************************************************************************!*\
  !*** G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/Common/construct.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.groupBy = exports.max = exports.min = exports.speed = exports.groundY = exports.groundX = exports.model = exports.CONTROLLERCENTER = exports.CONTROLLERRESTART = exports.CONTROLLERSTART = exports.CONTROLLERLEFT = exports.CONTROLLERDOWN = exports.CONTROLLERRIGHT = exports.CONTROLLERUP = void 0;

var _newArrowCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/newArrowCheck */ "./node_modules/@babel/runtime/helpers/newArrowCheck.js"));

var CONTROLLERUP = 1;
exports.CONTROLLERUP = CONTROLLERUP;
var CONTROLLERRIGHT = 2;
exports.CONTROLLERRIGHT = CONTROLLERRIGHT;
var CONTROLLERDOWN = 3;
exports.CONTROLLERDOWN = CONTROLLERDOWN;
var CONTROLLERLEFT = 4;
exports.CONTROLLERLEFT = CONTROLLERLEFT;
var CONTROLLERSTART = 10;
exports.CONTROLLERSTART = CONTROLLERSTART;
var CONTROLLERRESTART = 20;
exports.CONTROLLERRESTART = CONTROLLERRESTART;
var CONTROLLERCENTER = 99;
exports.CONTROLLERCENTER = CONTROLLERCENTER;
var triangle_1 = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 1, 0, 0], [1, 1, 1, 0]];
var triangle_2 = [[0, 0, 0, 0], [0, 1, 0, 0], [1, 1, 0, 0], [0, 1, 0, 0]];
var triangle_3 = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 0], [0, 1, 0, 0]];
var triangle_4 = [[0, 0, 0, 0], [1, 0, 0, 0], [1, 1, 0, 0], [1, 0, 0, 0]];
var matts = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 0, 0], [1, 1, 0, 0]];
var article_1 = [[1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0]];
var article_2 = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]];
var z_1 = [[0, 0, 0, 0], [1, 0, 0, 0], [1, 1, 0, 0], [0, 1, 0, 0]];
var z_2 = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 1, 1, 0], [1, 1, 0, 0]];
var RZ_1 = [[0, 0, 0, 0], [0, 1, 0, 0], [1, 1, 0, 0], [1, 0, 0, 0]];
var RZ_2 = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 0, 0], [0, 1, 1, 0]];
var L_1 = [[0, 0, 0, 0], [1, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]];
var L_2 = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 0], [1, 0, 0, 0]];
var L_3 = [[0, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0], [1, 1, 0, 0]];
var L_4 = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 0], [1, 1, 1, 0]];
var RL_1 = [[0, 0, 0, 0], [1, 1, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0]];
var RL_2 = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 0, 0, 0], [1, 1, 1, 0]];
var RL_3 = [[0, 0, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [1, 1, 0, 0]];
var RL_4 = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 0], [0, 0, 1, 0]];
var model = {
  triangle: [triangle_1, triangle_2, triangle_3, triangle_4],
  matts: [matts],
  article: [article_1, article_2],
  z: [z_1, z_2],
  rz: [RZ_1, RZ_2],
  L: [L_1, L_2, L_3, L_4],
  RL: [RL_1, RL_2, RL_3, RL_4]
};
exports.model = model;
var groundX = 8;
exports.groundX = groundX;
var groundY = 16;
exports.groundY = groundY;
var speed = 300;
exports.speed = speed;

var min = function min(a, b) {
  var _this = this;

  var result = a[0][b];
  a.forEach(function (t) {
    (0, _newArrowCheck2["default"])(this, _this);
    result = t[b] > result ? result : t[b];
  }.bind(this));
  return result;
};

exports.min = min;

var max = function max(a, b) {
  var _this2 = this;

  var result = a[0][b];
  a.forEach(function (t) {
    (0, _newArrowCheck2["default"])(this, _this2);
    result = t[b] > result ? t[b] : result;
  }.bind(this));
  return result;
};

exports.max = max;

var groupBy = function groupBy(array, name) {
  var groups = {};
  array.forEach(function (o) {
    var group = JSON.stringify(o[name]);
    groups[group] = groups[group] || [];
    groups[group].push(o);
  });
  return groups;
};

exports.groupBy = groupBy;

/***/ }),

/***/ "G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.hml?entry":
/*!********************************************************************************************!*\
  !*** G:/gitee/HarmonyOS-apps/Tetris/entry/src/main/js/default/pages/index/index.hml?entry ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! !./lib/loader.js!../../../controller/pages/index/index.hml?name=ctr */ "./lib/loader.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\controller\\pages\\index\\index.hml?name=ctr")
var $app_template$ = __webpack_require__(/*! !./lib/json.js!./lib/template.js!./index.hml */ "./lib/json.js!./lib/template.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.hml")
var $app_style$ = __webpack_require__(/*! !./lib/json.js!./lib/style.js!./index.css */ "./lib/json.js!./lib/style.js!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.css")
var $app_script$ = __webpack_require__(/*! !./lib/script.js!./node_modules/babel-loader?presets[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=E:/HarmonyOSIDE/SDK/js/2.1.1.21/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!./index.js */ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=E:\\HarmonyOSIDE\\SDK\\js\\2.1.1.21\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!G:\\gitee\\HarmonyOS-apps\\Tetris\\entry\\src\\main\\js\\default\\pages\\index\\index.js")

$app_define$('@app-component/index', [], function($app_require$, $app_exports$, $app_module$) {

$app_script$($app_module$, $app_exports$, $app_require$)
if ($app_exports$.__esModule && $app_exports$.default) {
$app_module$.exports = $app_exports$.default
}

$app_module$.exports.template = $app_template$

$app_module$.exports.style = $app_style$

})
$app_bootstrap$('@app-component/index',undefined,undefined)

/***/ })

/******/ });