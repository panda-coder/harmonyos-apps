import {groundX,groundY,model,CONTROLLERCENTER,CONTROLLERRIGHT,CONTROLLERDOWN,CONTROLLERLEFT,min,max,groupBy,CONTROLLERSTART,CONTROLLERRESTART,speed} from "../../../Common/construct.js"
import {jsCallJavaAbility} from '../../../Common/JsCallJavaAbilityUtils.js'
export default {
    data: {
        offsetX:0,
        offsetY:-3,
        modelChildIndex:-1,
        timeOut:null,
        isShowController:false,
        valueModel:Object.values(model),
        score:0,
        modelIndex:-1,
        nextModelInfo:{},
        nextModel:[],
        positionResult:[],
        modelCount:Object.keys(model).length,
        isStart:false,
        console:''
    },
    shareData:{
        ctrTouched:-1,
    },
    computed:{
        isGameOver(){
            let num=this.positionResult.filter(t=>t.y<=0).length
            return num>0;
        },
        positions(){
            let result=[]
            for(let z=0;z<groundY;z++){
                for(let i=0;i<groundX;i++){
                    result.push({
                        positionX:i,
                        positionY:z,
                        value:(this.positionResult.filter(t=>(t.x==i&&t.y==z)).length>0 || this.tempModel.filter(t=>(t.x==i && t.y==z)).length>0)?1:0
                    })
                }
            }

            return result;
        },
        grid_x(){
            let result="";
            for(let i=0;i<groundX;i++){
                result+=" 1fr";
            }
            return result;
        },
        grid_y(){
            let result="";
            for(let i=0;i<groundY;i++){
                result+=" 1fr";
            }
            return result;
        },
        tempModel(){
            let result=[];
            if(this.modelIndex<0)
                return [];
            let positionX=groundX/2-1+this.offsetX;
            let positionY=this.offsetY;
            let temp=this.getModelType(this.modelIndex,this.modelChildIndex);
            if(!temp)
                return [];
            let tmp=[];
            temp.forEach((t,a)=>{
                t.forEach((x,b)=>{
                    tmp.push({
                        positionX:b,
                        positionY:a,
                        value:x
                    })
                })
            })
            tmp.forEach(t=>{
                if(t.value==1){
                    result.push({
                        x:t.positionX+positionX,
                        y:t.positionY+positionY
                    })
                }
            })
            return result;
        }
    },
    onInit() {
        this.$watch('isGameOver','watchedGameOver');
        this.subCmd();
    },
    subCmd(){
        jsCallJavaAbility.subAbility("ControllerRemoteAbility",1002,(data)=>{
            let cmd= data.data.cmd;
            this.ctrTouchend(cmd);
        }).then(result=>{
            if(result.code==200){
                console.log("订阅成功");
            }
        });
    },
    watchedGameOver(newval,oldval){
        if(newval){
            clearInterval(this.timeOut)
        }
    },
    watchedPositionResult(){
        let isChanged=false;
        let tempData=JSON.parse(JSON.stringify(this.positionResult));
        let resultY=groupBy(tempData,"y");
        Object.values(resultY).forEach(t=>{
            if(t.length>=groundX){
                isChanged=true;
                let positionY=t[0].y;
                t.map(x=>{
                   let i= tempData.findIndex(a=>(a==x))
                   tempData.splice(i,1)
                })
                this.score+=100;
                tempData.filter(x=>x.y<positionY).map(x=>x.y+=1);
            }
        })
        isChanged &&(this.positionResult=tempData);
    },
    start(){
        if(this.isStart)
            return;
        clearInterval(this.timeOut);
        this.timeOut=this.goNext();
        this.isStart=true;
        this.getNewModel();
    },
    restart(){
        this.score=0;
        this.positionResult=[];
        this.isStart=true;
        clearInterval(this.timeOut);
        this.timeOut=this.goNext();
        this.getNewModel();
    },
    getNewModel(){
        if(this.isGameOver){
            clearInterval(this.timeOut);
            return;
        }
        this.offsetY=-3;
        this.offsetX=0;
        if(JSON.stringify(this.nextModelInfo)!="{}")
        {
            this.modelIndex=this.nextModelInfo.index ;
            this.modelChildIndex=this.nextModelInfo.childIndex
        }else{
            let index=parseInt(Math.random()*this.modelCount);
            let childNum=Object.values(model)[index].length;
            this.modelChildIndex=parseInt(Math.random()*childNum);
            this.modelIndex=index;
        }
        this.getNextModel();
    },
    goNext(){
        return setInterval(t=>{
            if(!this.checkGoNext(0,1)){
                this.positionResult=this.positionResult.concat(this.tempModel);
                this.watchedPositionResult();
                this.getNewModel();
            }else{
                this.moveDown();
            }
        },speed)
    },
    getModelType(index,childIndex){
        return this.valueModel[index][childIndex] ;
    },
    getNextModel(){
        let index=parseInt(Math.random()*this.modelCount);
        let childNum=this.valueModel[index].length;
        let childIndex=parseInt(Math.random()*childNum);
        let models=  this.getModelType(index,childIndex);
        this.nextModelInfo={
            index:index,
            childIndex:childIndex,
            models:models
        }
        this.nextModel=[];
       models.forEach((t,a)=>{
           t.forEach((x,b)=>{
               this.nextModel.push({
                   positionX:b,
                   positionY:a,
                   value:x
               })
           })
       })
    },
    //向左移动
    moveLeft(){
        if(this.isGoNext(CONTROLLERLEFT))
            this.offsetX-=1;
    },
    //向右移动
    moveRight(){
        if(this.isGoNext(CONTROLLERRIGHT))
            this.offsetX+=1;
    },
    //向下移动
    moveDown(){
        if(this.isGoNext(CONTROLLERDOWN))
            this.offsetY+=1;

    },
    //改变形态
    changeModel(){
        if(this.isGoNext(CONTROLLERCENTER)){
            let models=this.valueModel[this.modelIndex];
            this.modelChildIndex=(models.length-1)>this.modelChildIndex?(this.modelChildIndex+1):0;
        }

        this.continueAbility();
    },
    //判断能否进行下一步步骤
    isGoNext(cmd){
        let result=false;
        switch(cmd){
            case CONTROLLERRIGHT:
                result=this.checkGoNext(1,0);
                break;
            case CONTROLLERLEFT:
                result=this.checkGoNext(-1,0);
                break;
            case CONTROLLERDOWN:
                result=this.checkGoNext(0,1);
                break;
            case CONTROLLERCENTER:
                result=this.checkChangeModel();
                break;
        }
        return result;
    },
    checkGoNext(x,y,model){
        let result=true;
        model=model || this.tempModel;
        //判断x轴是否碰壁
        if(model.filter(t=>(t.x+x<0 || t.x+x>=groundX)).length>0)
               return result=false;
        //判断Y轴是否碰壁
        if(model.filter(t=>(t.y+y>=groundY)).length>0)
                return result=false;
         //算出需要计算是否碰撞的范围
        let position={
            minX:min(this.tempModel,"x")+x,
            maxX:max(this.tempModel,"x")+x,
            minY:min(this.tempModel,"y")+y,
            maxY:max(this.tempModel,"y")+y
        }
        //获取需要计算的矩阵
        let data=this.positionResult.filter(t=>((t.x>=position.minX && t.x<=position.maxX && t.y>=position.minY && t.y<=position.maxY) ))
        //计算是否碰撞，碰撞返回false
        if(data.length>0)
        {
            for(let i=0;i<data.length;i++){
                let length= this.tempModel.filter(a=>a.x+x==data[i].x&& a.y+y==data[i].y).length;
                if(length>0)
                    return result=false;
            }
        }
        return result;
    },
    //判段是否可以变换形态
    checkChangeModel(){
        let models=this.valueModel[this.modelIndex];
        let index=(models.length-1)>this.modelChildIndex?(this.modelChildIndex+1):0;
        let nextTypeModel=models[index];
        let nextModelPosition=[];
        let nextModel=[];
        let positionX=groundX/2-1+this.offsetX;
        let positionY=0+this.offsetY;
        nextTypeModel.forEach((t,a)=>{
            t.forEach((x,b)=>{
                nextModel.push({
                    positionX:b,
                    positionY:a,
                    value:x
                })
            })
        })
        nextModel.forEach(t=>{
            if(t.value==1){
                nextModelPosition.push({
                    x:t.positionX+positionX,
                    y:t.positionY+positionY
                })
            }
        })
       return this.checkGoNext(0,0,nextModelPosition);
    },
    //控制组件回传事件
    ctrTouchend(cmd){
        let data=cmd;
        switch(data){
            case CONTROLLERRIGHT:
                this.moveRight();break;
            case CONTROLLERLEFT:
                this.moveLeft();break;
            case CONTROLLERDOWN:
                this.moveDown();break;
            case CONTROLLERCENTER:
                this.changeModel();break;
            case  CONTROLLERSTART:
                this.start();break;
            case CONTROLLERRESTART:
                this.restart();break;
            case 100:{
                this.isShowController= !this.isShowController;break;
            }
        }
    },
}